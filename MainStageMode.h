#pragma once
#include <vector>

#include "Timer.h"

#include "ModeInterface.h"   //For mode interface
#include "Sprite.h"          //For making sprite vector arrays
#include "EntityInterface.h" //For making entity vector arrays

//Forward declarations
class Player;

enum class MODE_STATE : short int
{
    PLAYING, GAME_OVER
};

/*
    First Stage for the game.
*/
class MainStageMode : public iMode
{
public:
    //Mode handle
    inline static const std::string MODE_NAME = "MAIN_STAGE";

    MainStageMode();
    ~MainStageMode();

    //Parent overrides
    void Enter() override;
    bool Exit() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }

    //Get the stage objects array so that objects can interact with the elements for collision
    std::vector<iEntity*>& GetStageObjects() { return stageObjects_; }

    //Return the play area for the level
    RECTF& GetPlayArea();

    //Return Player pointer at index
    iEntity* GetPlayer(size_t index);

private:

    //Containers for our different object types (backgrounds, bodies, projectiles etc)
    std::vector<Sprite> backgroundScene_;
    std::vector<iEntity*> gameObjects_;
    std::vector<iEntity*> stageObjects_;
    std::vector<iEntity*> playerObjects_;

    //Spritefont for drawing the score
    DirectX::SpriteFont* font1_ = nullptr;

    //
    RECTF outOfBoundsArea_;

    Timer debugTimer_;
    //Delay player inputs for a short delay
    Timer actionDelayTimer_;

    /////////////////
    /// Collision ///
    /////////////////

    //Parent function
    void ManageCollisions();
    //Manage inter-player collisions
    void PlayerCollisons();


    //////////////
    /// Combat ///
    //////////////

    //Manage adding hit to counter for defending player, and process if they should stagger or not
    void ManageHitCounterInteraction(Player* attPlayer, Player* defPlayer);


    ////////////////////////
    /// Condition Checks ///
    ////////////////////////

    //Parent function for player status checks
    void CheckPlayerStatuses();
    //Check playerHP
    void CheckPlayerHPStatus();
    //Check each life count for each player
    void CheckLivesCount();
    //Go through each player and check position against OOB values
    void CheckOutOfBounds();
    //Sub function to check if the player is out of bounds
    bool IsPlayerOutOfBounds(Player* player);


    ////////////////////
    /// Initialising ///
    ////////////////////

    //Parent function
    void Init();
    //Reserve Vector Memory
    void ReserveMemory();
    //Allocates new objects for each container.
    void InsertStageObjects();
    //Setup background
    void InitialiseBackground();
    //Setup the initial game conditions
    void ConfigureGameState();

    /////////////////
    //// Health  ////
    /////////////////
    void HealthUpdate();
    void HealthInit();

    /////////////////
    /// Utilities ///
    /////////////////


    //Add a object into select container
    void Add(std::vector<iEntity*>& container, iEntity* object);
    //Delete a object in the container 
    void Remove(std::vector<iEntity*>& container, iEntity* object);
    //Clean up
    void Release();
};