#pragma once
#include "EntityInterface.h"        //Object Parent
#include "Sprite.h"                 //For sprites
#include "Player.h"
//Health display frames
const RECTF healthFrames[]
{
	{70,755,460,1000}, //HP frame 1
	{100,517,447,740}, //HP frame 2
	{756,562,850,572}, //life counter
	{740,592,966,600}, //HP bar
};

class HealthDisplay : public iEntity
{
public:
	HealthDisplay();
	~HealthDisplay();

	void Update(float dTime) override;
	void Render(float dTime, DirectX::SpriteBatch& batch) override;

	//Process Hp and life count bars
	void ProcessHealth1(HealthDisplay* bar1, Player* player1);
	void ProcessHealth2(HealthDisplay* bar2, Player* player2);
	void ProcessLife1(HealthDisplay* life1, Player* player1);
	void ProcessLife2(HealthDisplay* life2, Player* player2);

	std::vector<Sprite>healthDisplay_;
private:
	void Initialise();
	void HealthSetup();
};

