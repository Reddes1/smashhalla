#pragma once

#include "Sprite.h"     
#include "Game.h"


struct HealthSys
{
    float health = 100;
    float damageDealt = 0;
    float damageMultiplier = 1;
};
/*
Parent for any game object. Provides multiple interfaces for health, damage and activity as well
as a default sprite. Initialise derived classes with D3D reference.
*/
class iEntity
{
public:
    iEntity(MyD3D& d3d)
        : spr_(d3d)
    {}
    virtual ~iEntity() {}

    //////////////////////////
    /// Override Functions ///
    //////////////////////////

    //Mandatory object update function
    virtual void Update(float dTime) = 0;

    //Draw the sprite
    virtual void Render(float dTime, DirectX::SpriteBatch& batch) { spr_.Draw(batch); }
    //Get single damage value
    virtual float GetDamageValue() { return data_.damageValue; }
    //Set single damage values
    virtual void SetDamageValue(float newValue) { data_.damageValue = newValue; }
    //Refresh/Reset certain values of the object (like hp)
    virtual void ResetVariables() { }


    /////////////////
    /// Collision ///
    /////////////////

    /*
        Return basic collision values for Rect/Cir. Vector4(PosX, PosY, DimX, DimY).
        Uses ScreenSize(Derived from Texture Rect, only use if
        texture rect is accurate to sprite visuals).
    */
    Vector4 Get2DBasicBoxCollider();
    /*
        Return box collider values (Using Set2DBoxCollisionDimensions values).
        Vector4(PosX, PosY, 2DColX, 2DColY)
    */
    Vector4 Get2DDefinedBoxCollider();
    //Get 2D Collision Width/Height Dimensions
    Vector2 Get2DCollisionDimensions() const { return colData_.collisionBox2D; }
    //Set 2D Collision Dimensions
    void Set2DCollisionDimensions(Vector2& dims) { colData_.collisionBox2D = dims; }
    
    //Return 2D vertex data for complex collision.
    std::vector<Vector2>Get2DVertexData() const { return vertexData_; }

    /////////////
    /// Flags ///
    /////////////

    //Return the active state of the object
    bool const GetIsActive() const { return flagData_.isObjectActive; }
    //Set if the object is active or not
    void SetIsActive(bool state) { flagData_.isObjectActive = state; }

    //Enable/Disable inputs for the player
    void EnableInput(bool enableInput) { flagData_.enableInputs = enableInput; }


    /////////////////
    /// Utiliites ///
    /////////////////

    //Allow the object to return an id value defined by the child
    virtual const int GetID() { return INFINITE; }
    //Set optional ID for child classes
    virtual void SetID(int newID) { }

    //Set the scaling resolution for adjustments calculations (Default = 1920x1080)
    void SetScalingResolution(Vector2& scaling) { adj_.defaultRes = scaling; }
    //Update the scaling adjustments using current screen size
    void UpdateScalingAdjustments();

    //Get Adjust Values
    Vector2& GetAdjustValues() { return adj_.scaleAdj; }

    //Get a reference of the sprite object
    Sprite& GetSprite() { return spr_; }

    //////////////////
    /// Health Sys ///
    //////////////////
    void HealthDebug();
    HealthSys GetHealthSystem() { return healthSystem; }

    //const float GetHealthValue() { return GetHealthSystem().health; }

protected:
    //Default sprite object
    Sprite spr_;
    //If the sprite has any vertex data to be used alongside it (for specific collision etc), load into here.
    std::vector<Vector2> vertexData_;
    HealthSys healthSystem;
    //Store common data values
    struct
    {
        float healthValue = 1;
        float damageValue = 1;
    } data_;
    //Store common flags
    struct
    {
        bool isObjectActive = false;
        bool enableInputs = true;
    } flagData_;
    //Store central collision values
    struct
    {
        //Default values
        Vector2 collisionBox2D = { 0, 0 };
        Vector3 collisionBox3D = { 0, 0, 0 };
    } colData_;   
    //Store the values needed to adjust pixel sensitive values abstractly for use in calculations.
    struct
    {
        Vector2 spdAdj{ 1, 1 };
        Vector2 scaleAdj{ 1, 1 };
        Vector2 defaultRes{ 1920, 1080 };

    } adj_;



};


/////////////////////////
/// iEntity Utilities ///
/////////////////////////

//Find first object of a child type of iEntity in given vector container.
//Takes argument to check if object is active or not.
iEntity* FindFirst(std::vector<iEntity*>& container, const std::type_info& type, bool active);