#include "HealthDislpay.h"
#include "GameConstants.h"

HealthDislpay::HealthDislpay()
    :iEntity(Game::Get().GetD3D())
{
    Initialise();
}

HealthDislpay::~HealthDislpay()
{
}

void HealthDislpay::Update(float dTime)
{
}

void HealthDislpay::Render(float dTime, DirectX::SpriteBatch& batch)
{
    iEntity::Render(dTime, batch);
}

void HealthDislpay::Initialise()
{
    HealthFrameSetup();
}

void HealthDislpay::HealthFrameSetup()
{
    //reserve space
    healthDisplay_.reserve(4);

    //Handy Handles
    Game& game = Game::Get();
    MyD3D& d3d = game.GetD3D();
    WinUtil& winUtil = WinUtil::Get();

    //Get screen dims
    int width = winUtil.GetData().clientWidth;
    int height = winUtil.GetData().clientHeight;

    //Use iEntity sprite as a ghost container and set its parameters
    spr_.SetTextureRect(RECTF(0, 0, width, height));
    spr_.SetScale(Vector2(width / spr_.GetTextureRect().x, height / spr_.GetTextureRect().y));
    spr_.SetOrigin(Vector2(spr_.GetTextureRect() * 0.5f));
    spr_.SetPosition(Vector2(width * 0.5f, height * 0.5f));

    //Set up container 
    healthDisplay_.insert(healthDisplay_.begin(), FC::HEALTH_FRAME_COUNT, Sprite(game.GetD3D()));

    //Load texture and frames
    std::vector<RECTF> frames(healthFrames, healthFrames + sizeof(healthFrames) / sizeof(healthFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/sprites/UI/UI_Elements2.dds",
        "Health frame sprites", false, &frames);

    //configure health frame
    for (int i(0); i < FC::HEALTH_FRAME_COUNT; ++i)
    {
        healthDisplay_[i].SetTexture(*p);
        healthDisplay_[i].SetOrigin(Vector2(frames[0].right * 0.5f, frames[0].bottom * 0.5f));
        healthDisplay_[i].SetScale(spr_.GetScale());
        healthDisplay_[i].SetPosition(Vector2(width * 0.1f, height * 0.1f));
    }
}

void HealthDislpay::ProcessHelathBar1(HealthDislpay* bar1, Player* player1)
{
}

void HealthDislpay::ProcessHealthBar2(HealthDislpay* bar2, Player* player2)
{
}

void HealthDislpay::ProcessLifeCount1(HealthDislpay* life1, Player* player1)
{
}

void HealthDislpay::ProcessLifeCount2(HealthDislpay* life2, Player* player2)
{
}
