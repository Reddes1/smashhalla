#pragma once
#include <DDSTextureLoader.h>
#include <filesystem>       //C++17 

#include <string>           //strings
#include <vector>           //Array Type
#include <unordered_map>    //Container Type
#include <assert.h>         //Error Checking
#include <d3d11.h>          //DX11

#include "D3DUtil.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

class TexCache
{
public:

    TexCache()
        //Set initial expected append path
        :assetPathAppend_("data/")
    {}
    ~TexCache()
    {
        //Call release just incase not explicitly called
        Release();
    }

    //Setup data cashe to hold filename and pointer to memory
    struct Data {
        Data() = default;
        //Constructer for uses in texture loader
        Data(const std::string& fName, ID3D11ShaderResourceView* pT, const Vector2& dim)
            : fileName(fName), pTex(pT), texDim(dim)
        {}

        ID3D11ShaderResourceView* pTex = nullptr;
        std::string fileName;
        std::string nickName;
        DirectX::SimpleMath::Vector2 texDim;
    };
    //Clean up
    void Release();

    //Check if texture is new, otherwise find the texture and return a handle to it
    ID3D11ShaderResourceView* LoadTexture(ID3D11Device* pDevice, const std::string& fileName,
        const std::string& texName = "", bool appendPath = true);
    //Set appended path if needed
    void SetAssetPath(const std::string& filePath)
    {
        assetPathAppend_ = filePath;
    }
    //Slow method of fetching a texture by handle
    Data& Get(ID3D11ShaderResourceView* pTex);

private:
    
    typedef std::unordered_map<std::string, Data> TexMap;
    
    //Texture data array
    TexMap mCache_;

    std::string assetPathAppend_;

    //Get and store the dimensions of the texture
    Vector2 GetDimensions(ID3D11ShaderResourceView* pTex);
};