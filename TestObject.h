#pragma once

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class

const Vector2 verticeData[]
{
    { 256, 11 },
    { 0, 198 },
    { 97, 498 },
    { 414, 498 },
    { 511, 198 },
    { 256, 11 }
};

/*
    Test Object, No Special attributes but good for testing collision value at the moment.
*/

class TestObject : public iEntity
{
public:

    TestObject();
    ~TestObject()
    {}

    //Parent overrides
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ResetVariables() override;

private:

    //Initialise block
    void Initialise();


};