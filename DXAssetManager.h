#pragma once
#include <string>             //strings
#include <vector>             //Array Type
#include <unordered_map>      //Container Type
#include <assert.h>           //Error Checking
#include <d3d11.h>            //DX11
#include <filesystem>         //file load system for load texture

#include "DDSTextureLoader.h" //DX Loader
#include "D3DUtil.h"          //Dubug tools, RECTF

using namespace DirectX;
using namespace DirectX::SimpleMath;

class DXAssetManager
{
public:

    ~DXAssetManager() { Release(); }
    //Setup data cashe to hold filename and pointer to memory
    struct Data {
        Data() = default;
        //No frames
        Data(const std::string& fName, ID3D11ShaderResourceView* pT, const Vector2& dim)
            : fileName(fName), pTex(pT), dim(dim)
        {
            frames.clear();
        }
        //Has frames
        Data(const std::string& fName, ID3D11ShaderResourceView* p, const DirectX::SimpleMath::Vector2& _dim, const std::vector<RECTF>* _frames)
            :fileName(fName), pTex(p), dim(_dim)

        {
            if (_frames)
                frames = *_frames;
        }
        ID3D11ShaderResourceView* pTex = nullptr;
        std::string fileName;
        std::string nickName;
        Vector2 dim = { 0 , 0 };
        std::vector<RECTF> frames;
    };

    //Clean up
    void Release();

    //Check if texture is new, otherwise find the texture and return a handle to it
    ID3D11ShaderResourceView* LoadTexture(ID3D11Device* pDevice, const std::string& fileName, 
        const std::string& texName = "", bool appendPath = true, const std::vector<RECTF>* _frames = nullptr);


    //Set appended path if needed
    void SetAssetPath(const std::string& filePath)
    {
        assetAppendPath_ = filePath;
    }

    //pull out a texture by nickname = fast
    Data& GetTexture(const std::string& texName)
    {
        return texCache_.at(texName);
    }

    //Slow method of fetching a texture by handle
    const Data& GetHandle(ID3D11ShaderResourceView* pTex);

private:
    //Texture data array
    typedef std::unordered_map<std::string, Data> TexMap;
    TexMap texCache_;

    //File path appender with assumed default
    std::string assetAppendPath_ = "data/";

    //Get and store the dimensions of the texture
    Vector2 GetDimensions(ID3D11ShaderResourceView* pTex);

};