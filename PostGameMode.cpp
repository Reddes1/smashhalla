#include "PostGameMode.h"

#include "GameConstants.h"

PostGameMode::PostGameMode()
{
    Initialise();
}

PostGameMode::~PostGameMode()
{
    Release();
}

void PostGameMode::Enter()
{
    textDelayTimer_.ResetTimer();
}

void PostGameMode::Update(float dTime)
{
    //Handle Handle
    Game& game = Game::Get();

    //Allow the player to go into another mode after a small time as passed
    if (textDelayTimer_.IsTimerPastMarker())
    {
        for (size_t i(0); i < XUSER_MAX_COUNT; ++i)
        {
            if (game.GetGP().IsConnected(i) && game.GetGP().IsPressed(i, XINPUT_GAMEPAD_START))
            {
                Game::Get().GetModeManager().SwitchMode("MAIN_STAGE");
            }
            else if (game.GetGP().IsConnected(i) && game.GetGP().IsPressed(i, XINPUT_GAMEPAD_BACK))
            {
                Game::Get().GetModeManager().SwitchMode("MAIN_MENU");
            }
        }
    }

    //Update timer
    textDelayTimer_.UpdateTimer(dTime);
}

void PostGameMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    for (auto& a : backgrounds_)
        a.Draw(batch);

    RenderText(batch);
}

void PostGameMode::ProcessKey(char key)
{

}

void PostGameMode::Initialise()
{
    //Setup background
    InitialiseBackground();

    //Set Font
    font1_ = new SpriteFont(&Game::Get().GetD3D().GetDevice(), L"data/fonts/algerian.spritefont");

    //Setup timer
    textDelayTimer_.SetTimeMark(2.5f);
}

void PostGameMode::InitialiseBackground()
{
    assert(backgrounds_.empty());
    //Reserve Space
    backgrounds_.reserve(2);

    //Handy Handles
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), 1, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/Main_Menu_Background.dds", "Main Menu Background", false);

    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetScale(Vector2(width / 1920.f,
        height / 1080.f));
}

void PostGameMode::RenderText(DirectX::SpriteBatch& batch)
{
    //Handy Handles
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;



    //Setup first message
    //Get player 1 score
    int score = Game::Get().GetScoreManager().GetActivePlayersScoreAtIndex(0);
    std::string message = "Player 1 Score: ";
    message.append(to_string(score));
    
    //Setup Player 1 Message
    Vector2 pos{ width * 0.33f, height * 0.35f };

    RECT textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));
    //Draw 1 (player 1)
    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0,
        Vector2((float)textDim.right * 0.5f, 2));

    //Setup Player 2 Message
    //Get player 2 score
    score = Game::Get().GetScoreManager().GetActivePlayersScoreAtIndex(1);
    message = "Player 2 Score: " /*Score Manager Value Here!*/;
    message.append(to_string(score));
    pos = { width * 0.66f, height * 0.35f };
    textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));

    //Draw 2 (player 2)
    font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0,
        Vector2((float)textDim.right * 0.5f, 2));
    
    //Setup Continue/Quit Message if timer allowed.
    if (textDelayTimer_.IsTimerPastMarker())
    {
        message = "Press Start to play Again, Press Back to Quit to Menu!";
        pos = { width * 0.5f, height * 0.75f };
        textDim = font1_->MeasureDrawBounds(message.c_str(), Vector2(0, 0));
        //Draw Continue/Quit
        font1_->DrawString(&batch, message.c_str(), pos, Colours::White, 0,
            Vector2((float)textDim.right * 0.5f, 2));
    }
}

void PostGameMode::Release()
{
    if (font1_ != nullptr)
        delete font1_;

    backgrounds_.clear();
}
