#include "EntityInterface.h"

Vector4 iEntity::Get2DBasicBoxCollider()
{
    Vector2 pos = spr_.GetPosition();
    Vector2 dim = spr_.GetScreenSize();
    Vector4 rect = Vector4(pos.x, pos.y, dim.x, dim.y);
    return rect;
}

Vector4 iEntity::Get2DDefinedBoxCollider()
{
    Vector2 pos = spr_.GetPosition();
    Vector2 dim = { colData_.collisionBox2D.x, colData_.collisionBox2D.y };
    Vector4 colBox = Vector4(pos.x, pos.y, dim.x, dim.y);
    return colBox;
}

void iEntity::UpdateScalingAdjustments()
{
    //Handy Handle
    WinUtil& win = WinUtil::Get();
    //Get current screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    adj_.scaleAdj = Vector2(width / adj_.defaultRes.x, height / adj_.defaultRes.y);
    adj_.spdAdj = Vector2(width / adj_.defaultRes.x, height / adj_.defaultRes.y);
}


void iEntity::HealthDebug()
{
    DBOUT("Damage = " << healthSystem.damageDealt);
    DBOUT("Health = " << healthSystem.health);
}

iEntity* FindFirst(std::vector<iEntity*>& container, const std::type_info& type, bool active)
{
    size_t i = 0;
    while (i < container.size() && (typeid(*container[i]) != type || container[i]->GetIsActive() != active))
        ++i;
    if (i >= container.size())
        return nullptr;
    return container[i];
}
