#include "HealthBars.h"

HealthBars::HealthBars()
    :iEntity(Game::Get().GetD3D())
{

}

HealthBars::~HealthBars()
{
}

void HealthBars::Update(float dTime)
{
}

void HealthBars::Render(float dTime, DirectX::SpriteBatch& batch)
{
    iEntity::Render(dTime, batch);
}

void HealthBars::Initialise()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Update scaling values to current screen size
    UpdateScalingAdjustments();

    //Load texture
    std::string filePath = "data/sprites/health/healthBar.dds";

    //image size
    const float SIZE = 256;

    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(),
        filePath, "Health Bar", false);

    spr_.SetOrigin(Vector2(SIZE / 2, SIZE / 2));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 0.5f, adj_.scaleAdj.y * 0.5f));
    spr_.SetPosition(Vector2(width * 0.1, height * 0.1));

}
