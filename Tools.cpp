#include "Tools.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

bool ValidateCharAz(char letter)
{
    {
        if ((letter >= 65 && letter <= 90) || (letter >= 97 && letter <= 122) || letter == 45)
            return true;
        else
            return false;
    }
}

int GetRandNumFromRange(int range)
{
    {
        srand(time(0));

        return ((rand() % range) + 1);
    }
}
