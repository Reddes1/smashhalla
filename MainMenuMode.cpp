#include "MainMenuMode.h"       //Owner

#include "GameConstants.h"      //consts namespaces

MainMenuMode::MainMenuMode()
{
    backgrounds_.reserve(2);

    //Inits
    InitBackground();
    InitMenu();
}

MainMenuMode::~MainMenuMode()
{
    //Delete menu object
    for (size_t i(0); i < menu_.size(); ++i)
    {
        delete menu_[i];
    }
    //Clear containers
    menu_.clear();
    backgrounds_.clear();
}

void MainMenuMode::Enter()
{
    //Update our game state
    Game::Get().SetGameState(GameState::MAIN_MENU_SCREEN);
}

void MainMenuMode::Update(float dTime)
{
    //Menu Interaction Here
    for (auto& m : menu_)
    {
        m->Update(dTime);
    }

}

void MainMenuMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render background
    for (auto& a : backgrounds_)
    {
        a.Draw(batch);
    }

    //Render Menu
    for (auto& a : menu_)
    {
        a->Render(dTime, batch);
    }
}

void MainMenuMode::ProcessKey(char key)
{
    switch (key)
    {
    case VK_ESCAPE: //Exit out
        PostQuitMessage(0);
        break;
    }
}

void MainMenuMode::InitMenu()
{
    //Add new object in
    MainMenuObj* obj_ = new MainMenuObj();
    menu_.insert(menu_.begin(), 1, obj_);
}

void MainMenuMode::InitBackground()
{
    assert(backgrounds_.empty());
    //Handy Handle
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgrounds_.insert(backgrounds_.begin(), FC::MAIN_MENU_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/Main_Menu_Background.dds", "Main Menu Background", false);

    //Configure sprite
    backgrounds_[0].SetTexture(*p);
    backgrounds_[0].SetScale(Vector2(width / 1920.f ,
        height / 1080.f));
}