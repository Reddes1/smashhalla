#include "D3DUtil.h"
#include <time.h> //time function

static float gTime = 0;

//Return clock value
float GetClock()
{
    return gTime;
}
//Reset the internal clock to zero
void SetClockTime(float value)
{
    gTime = value;
}
//Update clock counter
void AddSecToClock(float sec)
{
    gTime += sec;
}

float GetRandom(float min, float max)
{
    float res = (float)rand() / RAND_MAX;
    res = min + res * (max - min);
    return res;
}

void SeedRandom(int seed /*= -1*/)
{
    if (seed == -1)
        seed = (int)time(nullptr);

    srand(seed);
}


