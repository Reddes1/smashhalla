#pragma once
#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class

/*
    Simple Class object designed to operate as a simple rect style hitboxes with 
    additional utilities.
*/
class RectHitbox : public iEntity
{
public:
    RectHitbox()
        :iEntity(Game::Get().GetD3D())   
    {
        hasHitList_.reserve(12);
    }
    ~RectHitbox()
    {
        ClearObjectList();
    }

    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override { };

    bool IsObjectInList(iEntity* pointer);
    void ClearObjectList() { hasHitList_.clear(); }
    void DisableHitbox(bool clearList = true);

    std::vector<iEntity*>& GetHitList() { return hasHitList_; }

private:

    std::vector<iEntity*> hasHitList_;

    bool playerCollision = false;
    bool enemyColliison = false;
    bool collisionEvent = false;
    bool allowCollision = true;
};

inline bool RectHitbox::IsObjectInList(iEntity* pointer)
{
    for (std::vector<iEntity*>::iterator it = hasHitList_.begin(); it != hasHitList_.end(); ++it)
    {
        iEntity* lhs = *it;

        if (lhs == pointer)
        {
            return true;
        }
    }
    return false;
}

inline void RectHitbox::DisableHitbox(bool clearList)
{
    flagData_.isObjectActive = false;
    if (clearList)
        ClearObjectList();
}