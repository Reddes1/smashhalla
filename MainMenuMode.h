#pragma once
#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent

#include "Sprite.h"          //For making sprite vector arrays
#include "MainMenuObj.h"     //The menu object

/*
Main Menu class, manages navigation into the game proper and exit from it. Scalable to include option control but NYI.
*/
class MainMenuMode : public iMode
{
public:
    //Handle for changing the modes
    inline static const std::string MODE_NAME = "MAIN_MENU";

    MainMenuMode();
    ~MainMenuMode();

    //Parent overrides
    void Enter() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }

private:

    //Sprite/object containers
    std::vector<Sprite> backgrounds_;
    std::vector<iEntity*> menu_;

    //Initialise block
    void InitBackground();
    void InitMenu();

};