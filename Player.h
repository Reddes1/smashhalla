#pragma once

#include "MainStageMode.h"      //Mode

#include "Timer.h"              //Timer Utility

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "FlagSets.h"           //bool flag objects
#include "RectHitBox.h"         //Specialised hitbox
#include "GameConstants.h"

#define PLAYER_FP_BLUE "data/sprites/player/sprite_sheet_blue.dds"
#define PLAYER_FP_RED "data/sprites/player/sprite_sheet_red.dds"

const Vector2 attackHitboxes[]
{
    { 40, 20 },   //Punch 1
    { 36, 38 },   //Down attack
    { 50, 50 },   //Down Attack
    { 50, 50 }    //Left/Right Attack
};
//Player States
enum class PLAYER_STATE : short int
{
    ALIVE, DEAD, RESPAWNING
};

enum class MOVEMENT_STATE : short int
{
    STATIONARY, RUNNING, JUMPING, FALLING
};

enum class ATTACK_STATE : short int
{
    NOT_ATTACKING, GROUND_ATTACK, STATIC_ATT_LEFT, STATIC_ATT_RIGHT, MOVE_ATT_LEFT, MOVE_ATT_RIGHT,
    JUMPING_ATT, FALLING_ATT
};

/*
    Specialised player object designed for 2d fighter game.
*/
class Player : public iEntity
{
public:

    Player();
    //Give the player and ID (which determines what spriteset to assign to it on instantiation)
    Player(int playerID);
    ~Player()
    {}

    //Parent overrides
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ResetVariables() override;

    //Get the current player ID
    const int GetID() override { return playerID_; }
    //Set the Player ID
    void SetID(int newID) override { playerID_ = newID; }
    void SetMode(MainStageMode& mode) { ownerMode_ = &mode; }

    //Get the hitbox data
    RectHitbox* GetHitboxAtIndex(size_t index);

    //Get hitbox count
    size_t GetHitboxCount() { return hitboxes_.size(); }
    //Aaron comment here
    void ProcessAttackDmg(Player* attackingPlayer, Player* defendingPlayer);
    //Add to hit counter
    void AddToHitCounter();
    //Get hit count value
    const int GetHitCounter() const { return hits_; }
    //Flag the conditions of how the player is flagged
    void ProcessStaggerConditions(Player* attPlayer);
    
    void SetPlayerLives(int newLivesCount) { playerLives_ = newLivesCount; }
    const int GetPlayerLives() const { return playerLives_; }

    //Player Death Script
    void PlayerDeath();
    //Setup player
    void RespawnPlayer(bool freshGame = false);

    //Get the player state
    const PLAYER_STATE GetPlayerState() { return playerState_; }
    const ATTACK_STATE GetAttackState() { return attackState_; }

private:

    //Hold hitbox data here
    std::vector<RectHitbox> hitboxes_;

    //Custom origin positioning to adjust for certain frames (calculated in init)
    struct
    {
        Vector2 defaultOrigin;
    } originData_;

    //Flag containers
    InputFlags inputFlags_;
    MovDirFlags mdFlags_;
    ObjStateFlags objFlags_;
    GenericFlags8 canDamageFlags_;
    // 0 = Staggered, 1 = Stagger Left, 2 = Stagger Right
    GenericFlags8 staggerFlags_;

    //States
    PLAYER_STATE playerState_;
    MOVEMENT_STATE moveState_;
    ATTACK_STATE attackState_;

    //Mode handle
    MainStageMode* ownerMode_ = nullptr;

    //Player ID
    int playerID_ = 0;

    //Player Lives count
    int playerLives_ = 3;

    //Track how many hits the player has taken
    short unsigned hits_ = 0;

    //Timers
    Timer hitBuildUpWindow_;
    Timer staggerDuration_;
    Timer jumpDuration_;
    Timer respawnTimer_;

    ////////////////////////
    /// Control/Movement ///
    ////////////////////////


    //Reset frame sensitive flags
    void PreFrameFlagReset();
    //Check inputs and raise related flags
    void GetInputEvents();
    //Parent Update Function
    void UpdateMovement(float dTime);
    //Update Gamepad Inputs (Need implimenting)
    void UpdateGamepad(float dTime);
    //Update player movement specific to status condition (knockback etc)
    bool UpdateStatusMovement(float dTime, Vector2& playerPos);
    //Update horizontal movement if action flagged
    bool UpdateHorizontalMovement(float dTime, Vector2& playerPos, bool isMovePressed);
    //Manage vertial axis movement during jump or falling
    void UpdateVerticalMovement(float dTime, Vector2& playerPos);
    //Update actions like jumping etc
    void UpdateActions(float dTime);
    //Position Validation against existing platforms
    void ValidatePositionPlatforms();
    //Position Validation against window edges.
    void ValidatePositionWindow();



    //////////////////
    /// Animations ///    
    //////////////////

    //Parent update function
    void UpdateAnimation(float dTime);
    //Update Animation if under a certain status
    bool UpdateStatusAnimations();
    //Update movement animations
    void UpdateMovementAnimations();
    //Update attack animations
    bool UpdateAttackAnimations();
    //Update facing directions
    void UpdateFacingDirection();
    //Validate origin for different frames
    void ValidateOrigin();



    //////////////
    /// Combat ///
    //////////////

    //Main collision parent function
    void UpdateCollision();
    //Disable hitboxes and clear flags
    void PreCollisionCleanUp();
    //Set appropriate state using flagged actions
    void ProcessAttack();
    //Flag if hitboxes should be active based on frame
    void HitboxFlagCheck();
    //Position attack hitbox (just 1 attack atm)
    void PositionCollisionBox();
    //Check through hitbox lists and clear if inactive
    void CheckHitboxLists();



    ////////////
    /// Misc ///
    ////////////

    //Check flag conditions prior to main update block
    void PreUpdateFlagCheck();
    //Update logic independant timers
    void UpdateTimers(float dTime);
    //Check timer durations and reflag appropriately
    void PostFlagCheck();


    ////////////////////////
    /// Parameter Setups ///
    ////////////////////////





    ////////////////////
    /// Initialising ///
    ////////////////////

    //Parent function
    void Initialise();
    //Set player function
    void SetTexture();
    //Configure player timers
    void ConfigureTimers();
    //Run first time player setup
    void InitialiseDefaultPlayer(std::vector<RECTF>& frames);

    //Set player attack hitboxes
    void SetupHitboxes();



};