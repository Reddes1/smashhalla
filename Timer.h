#pragma once

/*
    Simple timer object with abilities to hold a timed value, updated with delta time or increments.
    Also supports Time Marker for timer <= || >= bool checks.
*/

class Timer
{
public:

    Timer()
        :timer_(0), timeMarker_(0)
    { }

    Timer(float timer, float marker)
        :timer_(timer), timeMarker_(marker)
    { }
    ~Timer() {}

    //Reset the current
    void ResetTimer() { timer_ = 0; }
    //Get current timer value
    float GetTimer() { return timer_; }
    //Update timer with value (ideally delta time)
    void UpdateTimer(float dTime) { timer_ += dTime; }
    //Increment Timer
    void IncrementTimer() { ++timer_; }
    //Decrement Timer
    void DecrementTimer() { --timer_; }

    //Set timer condition time
    void SetTimeMark(float markerTime) { timeMarker_ = markerTime; }
    const float GetTimeMark() { return timeMarker_; }

    //Condition check if the timer is passed the Time Marker
    bool IsTimerPastMarker() { return (timer_ >= timeMarker_); }
    //Condition check if the timer is before the Time Marker
    bool IsTimerBeforeMarker() { return (timer_ <= timeMarker_); }
    //Set the timer to equal time mark value
    void SetTimerEqualToMarker() { timer_ = timeMarker_; }

private:

    //Container
    float timer_;
    //Condition value
    float timeMarker_;
};