#include "LivesCount.h"

LivesCount::LivesCount()
	:iEntity(Game::Get().GetD3D())
{
	Initialise();
}

LivesCount::~LivesCount()
{
}

void LivesCount::Update(float dTime)
{
}

void LivesCount::Render(float dTime, DirectX::SpriteBatch& batch)
{
    iEntity::Render(dTime, batch);
}

void LivesCount::ProcessLives1(LivesCount* lives1, Player* player1)
{
    switch (player1->GetPlayerLives())
    {
    case(0):
        lives1->spr_.SetTextureRect(RECTF(756, 562, 756, 572));
        break;
    case(1):
        lives1->spr_.SetTextureRect(RECTF(756, 562, 785, 572));
        break;
    case(2):
        lives1->spr_.SetTextureRect(RECTF(756, 562, 819, 572));
        break;
    case(3):
        lives1->spr_.SetTextureRect(RECTF(756, 562, 850, 572));
        break;
    }
}

void LivesCount::ProcessLives2(LivesCount* lives2, Player* player2)
{
    switch (player2->GetPlayerLives())
    {
    case(0):
        lives2->spr_.SetTextureRect(RECTF(756, 562, 756, 572));
        break;
    case(1):
        lives2->spr_.SetTextureRect(RECTF(756, 562, 785, 572));
        break;
    case(2):
        lives2->spr_.SetTextureRect(RECTF(756, 562, 819, 572));
        break;
    case(3):
        lives2->spr_.SetTextureRect(RECTF(756, 562, 850, 572));
        break;
    }
}

void LivesCount::Initialise()
{
	LivesCountInit();
}

void LivesCount::LivesCountInit()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Update scaling values to current screen size
    UpdateScalingAdjustments();

    //Load texture and frames
    std::vector<RECTF> frames(FD::UIAtlasOneFrames, FD::UIAtlasOneFrames + sizeof(FD::UIAtlasOneFrames) / sizeof(FD::UIAtlasOneFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/UI/UI_Elements2.dds",
        "UI Atlas", false, &frames);
    const Vector2 SIZE = {94, 10};

    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[6]);
    spr_.SetOrigin(Vector2(SIZE.x / 2, SIZE.x / 2));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 1.f, adj_.scaleAdj.y * 1.f));
    spr_.SetPosition(Vector2(width * 0.1f, height * 0.2f));


}
