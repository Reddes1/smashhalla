#include "Sprite.h"


void Animator::Initialise(int start, int stop, float speed, bool loop, bool reverseAnim)
{
    animElapsedTime_ = 0;
    animStart_ = start;
    animEnd_ = stop;
    animSpeed_ = speed;
    loopAnim_ = loop;
    spr_.SetFrame(animStart_);
    animCurrent_ = animStart_;

    if (reverseAnim)
        reverseAnim_ = true;
    else
        reverseAnim_ = false;
}

void Animator::Update(float dTime)
{
    if (!playAnim_)
        return;
    animElapsedTime_ += dTime;
    if (animElapsedTime_ > (1.f / animSpeed_))
    {
        //Reset Animation counter
        animElapsedTime_ = 0;
        //Switch how we process the animation
        switch (reverseAnim_)
        {
        case(false): //Increment and process animation the regular way
            animCurrent_++;
            if (animCurrent_ > animEnd_)
            {
                if (loopAnim_)
                    animCurrent_ = animStart_;
                else
                    animCurrent_ = animEnd_;
            }
            break;
        case(true): //Decrement and process animation in reverse
            animCurrent_--;
            if (animCurrent_ < animStart_)
            {
                if (loopAnim_)
                    animCurrent_ = animEnd_;
                else
                    animCurrent_ = animStart_;
            }
            break;
        }
        //Set new frame
        spr_.SetFrame(animCurrent_);
    }

}

void Animator::SetAnimationReverse(bool isReversed)
{
    reverseAnim_ = isReversed;
}

void Animator::SetFrame(int frameID)
{
    //Is the frame within the total amount of frames given?
    assert(frameID >= animStart_ && frameID <= animEnd_);
    animCurrent_ = frameID;
    spr_.SetFrame(animCurrent_);
}

void Animator::AddNewAnimation(AnimData newAnim)
{
    newAnim.animID = anims_.size();
    anims_.push_back(newAnim);
}

void Animator::AddNewAnimation(int newAnimStart, int newAnimEnd, int newAnimSpeed, std::string newAnimName)
{
    AnimData newAnim;
    newAnim.animID = anims_.size();
    newAnim.animStartFrame = newAnimStart;
    newAnim.animEndFrame = newAnimEnd;
    newAnim.animationSpeed = newAnimSpeed;
    newAnim.animName = newAnimName;
    anims_.push_back(newAnim);
}

void Animator::SetAnimation(int indexID, bool play, bool loop, bool reverseAnim)
{
    assert(indexID >= 0 && indexID <= anims_.size());
    
    //Get a hold of the requested animation
    AnimData& handle = anims_[indexID];
    //Load animation information
    animStart_ = handle.animStartFrame;
    animEnd_ = handle.animEndFrame;
    animSpeed_ = handle.animationSpeed;
    animCurrent_ = animStart_;
    //Set play type parameters
    playAnim_ = play;
    loopAnim_ = loop;
    reverseAnim_ = reverseAnim;

    //Set the currently playing animation to no longer playing
    if(currentAnim != nullptr)
        currentAnim->isAnimActive = false;
    //Update the current anim handle
    currentAnim = &handle;

    //Set new current to active
    currentAnim->isAnimActive = true;

    //Set the starting frame
    SetFrame(animStart_);
}

Animator& Animator::operator=(const Animator& rSide)
{
    animStart_ = rSide.animStart_;
    animEnd_ = rSide.animEnd_;
    animCurrent_ = rSide.animCurrent_;
    animSpeed_ = rSide.animSpeed_;
    animElapsedTime_ = rSide.animElapsedTime_;
    loopAnim_ = rSide.loopAnim_;
    playAnim_ = rSide.playAnim_;
    return *this;
}


void Sprite::Draw(SpriteBatch& batch)
{
    batch.Draw(tex_, pos_, &(RECT)texRect_, colour_, rot_, origin_, scale_,
        (SpriteEffects)spriteEffectID_, depth_);
}

void Sprite::SetTexture(ID3D11ShaderResourceView& tex, const RECTF& texRect)
{
    tex_ = &tex;
    texRect_ = texRect;
    texData_ = &d3d_.GetCache().GetHandle(tex_);

    //If the texture has no defined rect
    if (texRect_.left == texRect_.right && texRect_.top == texRect_.bottom)
    {
        //Set rect from whole texture size
        SetTextureRect(RECTF{ 0, 0, texData_->dim.x, texData_->dim.y });
    }

}

void Sprite::SetTextureRect(const RECTF& texRect)
{
    texRect_ = texRect;
    UpdateScreenSize();
}

void Sprite::SetPosition(Vector2& pos)
{
    pos_ = pos;
}
void Sprite::SetVelocity(const Vector2& vel)
{
    vel_ = vel;
}
void Sprite::SetScale(const Vector2& scale)
{
    scale_ = scale;
    UpdateScreenSize();
}
void Sprite::SetOrigin(const Vector2& origin)
{
    origin_ = origin;
}
void Sprite::SetColour(const Vector4& colour)
{
    colour_ = colour;
}
void Sprite::SetRotation(const float rot)
{
    rot_ = rot;
}
void Sprite::SetDepth(const float depth)
{
    depth_ = depth;
}
void Sprite::SetSpriteEffect(SprEffects choice)
{
    spriteEffectID_ = choice;
}
void Sprite::ScrollTexture(float x, float y)
{
    texRect_.left += x;
    texRect_.right += x;
    texRect_.top += y;
    texRect_.bottom += y;
}

void Sprite::SetFrame(int frameID)
{
    const DXAssetManager::Data& data = d3d_.GetCache().GetHandle(tex_);
    SetTextureRect(data.frames.at(frameID));
}

//Called during any function that would alter what is seen on screen.
void Sprite::UpdateScreenSize()
{
    if (texRect_.right == 0)
        screenSize_ = (scale_ * texData_->dim);
    else
        screenSize_ = scale_ * Vector2(texRect_.right - texRect_.left, texRect_.bottom - texRect_.top);
}

Vector2 Sprite::GetPosition() const
{
    return pos_;
}
Vector2 Sprite::GetVelocity() const
{
    return vel_;
}
Vector2 Sprite::GetScale() const
{
    return scale_;
}
Vector2 Sprite::GetOrigin() const
{
    return origin_;
}
float Sprite::GetRotation() const
{
    return rot_;
}
float Sprite::GetDepth() const
{
    return depth_;
}
Vector4 Sprite::GetColour() const
{
    return colour_;
}
Vector2 Sprite::GetTextureRect() const
{
    Vector2 tempRect(texRect_.right - texRect_.left, texRect_.bottom - texRect_.top);

    return tempRect;
}
//Get the size of the sprite with scale accounted for (i.e what you see on the screen)
Vector2 Sprite::GetScreenSize() const
{
    return screenSize_;
}

Vector4 Sprite::GetCollisionValues() const
{
    Vector4 rect = Vector4(pos_.x, pos_.y, screenSize_.x, screenSize_.y);
    return rect;
}

//---------Operators
Sprite& Sprite::operator=(const Sprite& rSide)
{
    tex_ = rSide.tex_;
    texRect_ = rSide.texRect_;
    screenSize_ = rSide.screenSize_;

    pos_ = rSide.pos_;
    vel_ = rSide.vel_;
    scale_ = rSide.scale_;
    origin_ = rSide.origin_;

    colour_ = rSide.colour_;
    rot_ = rSide.rot_;
    depth_ = rSide.depth_;
    animation_ = rSide.animation_;

    texData_ = rSide.texData_;
    spriteEffectID_ = rSide.spriteEffectID_;
    return *this;
}
