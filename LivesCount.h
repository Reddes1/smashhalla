#pragma once

#include "MainStageMode.h"      //Mode

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "Player.h"
#include "GameConstants.h"
class LivesCount : public iEntity
{
public:
	LivesCount();
	~LivesCount();
	void Update(float dTime) override;
	void Render(float dTIme, DirectX::SpriteBatch& batch) override;

	void ProcessLives1(LivesCount* lives1, Player* player1);
	void ProcessLives2(LivesCount* lives2, Player* player2);
private:
	void Initialise();
	void LivesCountInit();
};
