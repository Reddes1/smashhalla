#pragma once
#include "MainStageMode.h"      //Mode

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "Player.h"
#include "GameConstants.h"
class HealthFrame : public iEntity
{
public:
	HealthFrame();
	~HealthFrame();

	void Update(float dTime) override;
	void Render(float dTime, DirectX::SpriteBatch& batch) override;
private:
	void Initialise();
	void HealthFrameInit();
};

