#pragma once
#include "Tools.h"						//Utilites
#include <string>

#include "D3D.h"
#include "SpriteBatch.h"



/*
				Flag set that container useful flags for tracking when the mode has been accessed (first time entering),
				or when it might be left (Is it leaving to pause or because gameover).
*/

class iEntity;

struct FlagSetStates
{
				FlagSetStates()
								:isNewGame(true), isPaused(false), isGameOver(false), isRestarting(false),
								isComplete(false), isLoaded(false), stopInput(false)
				{}
				bool isNewGame : 1;
				bool isPaused : 1;
				bool isGameOver : 1;
				bool isRestarting : 1;
				bool isComplete : 1;
				bool isLoaded : 1;
				bool stopInput : 1;

};

/*
				Mode interface that any particular mode of a game (main menu, game over etc) will use.
*/
class iMode
{
public:
				virtual ~iMode() {}
				/*
				Called on the old mode when switching to the new one
				The switch won't complete until we return true, gives time for
				effects like fading out.
				*/
				virtual bool Exit() { return true; }

				//called on the new mode just once when it first activates
				virtual void Enter() {}

				//once active this is called repeatedly, contains the logic of the mode
				virtual void Update(float dTime) = 0;

				//used by a mode to render itself
				virtual void Render(float dTime, DirectX::SpriteBatch& batch) = 0;

				//get a mode's name
				virtual std::string GetModeName() const = 0;

				//pass WM_CHAR key messages to a mode
				virtual void ProcessKey(char key) {};

				//Get mode flags, usefull if another active mode needs to change the behaviour of another
				FlagSetStates& GetFlags() { return modeStates_; }

protected:
				
				/*
								Useful flagset for checking the state of the mode, how we are entering or leaving it, or how to
								process an internal state change (did the player die etc).
				*/	
				FlagSetStates modeStates_;

};