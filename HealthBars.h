#pragma once
#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "TestStageMode.h"
#include "GameConstants.h"
class HealthBars : public iEntity
{
public:
	HealthBars();
	~HealthBars();

	//parent overrides
	void Update(float dTime) override;
	void Render(float dTime, DirectX::SpriteBatch& batch) override;
	void Initialise();
private:

};

