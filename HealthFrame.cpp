#include "HealthFrame.h"

HealthFrame::HealthFrame()
	:iEntity(Game::Get().GetD3D())
{
    Initialise();
}

HealthFrame::~HealthFrame()
{
}

void HealthFrame::Update(float dTime)
{
}

void HealthFrame::Render(float dTime, DirectX::SpriteBatch& batch)
{
	iEntity::Render(dTime, batch);
}

void HealthFrame::Initialise()
{
	HealthFrameInit();
}

void HealthFrame::HealthFrameInit()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Update scaling values to current screen size
    UpdateScalingAdjustments();

    //Load texture and frames
    std::vector<RECTF> frames(FD::UIAtlasOneFrames, FD::UIAtlasOneFrames + sizeof(FD::UIAtlasOneFrames) / sizeof(FD::UIAtlasOneFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/UI/UI_Elements2.dds",
        "UI Atlas", false, &frames);
    const Vector2 SIZE = { 390,245 };

    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[3]);
    spr_.SetOrigin(Vector2(SIZE.x / 2, SIZE.y / 2));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 1.f, adj_.scaleAdj.y * 1.f));
    spr_.SetPosition(Vector2(width * 0.1f, height * 0.1f));
}
