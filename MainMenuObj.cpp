#include "MainMenuObj.h"
#include "GameConstants.h"

#define PLAY_GAME_DEST "MAIN_STAGE"
#define HOW_TO_PLAY_DEST "MAIN_STAGE"

MainMenuObj::MainMenuObj()
    :iEntity(Game::Get().GetD3D())
{
    Initialise();
}

MainMenuObj::~MainMenuObj()
{

}

void MainMenuObj::Initialise()
{
    //Reserve space
    menuSprites_.reserve(4);

    //Handy Handles
    Game& game = Game::Get();
    MyD3D& d3d = game.GetD3D();
    WinUtil& winUtil = WinUtil::Get();

    //Get screen dims
    int width = winUtil.GetData().clientWidth;
    int height = winUtil.GetData().clientHeight;

    //Use the iEntity sprite as a ghost container and set its parameters
    spr_.SetTextureRect(RECTF(0, 0, 1366, 768));
    spr_.SetScale(Vector2(width / spr_.GetTextureRect().x, height / spr_.GetTextureRect().y));
    spr_.SetOrigin(Vector2(spr_.GetTextureRect() * 0.5f));
    spr_.SetPosition(Vector2(width * 0.5f, height * 0.5f));

    //Set up container buttons
    menuSprites_.insert(menuSprites_.begin(), FC::MENU_BUTTON_COUNT, Sprite(game.GetD3D()));

    //Load texture and frames
    std::vector<RECTF> frames(FD::UIAtlasOneFrames, FD::UIAtlasOneFrames + sizeof(FD::UIAtlasOneFrames) / sizeof(FD::UIAtlasOneFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/UI/UI_Elements2.dds",
        "UI Atlas", false, &frames);

    //Configure the menu
    for (int i(0); i < FC::MENU_BUTTON_COUNT; ++i)
    {
        menuSprites_[i].SetTexture(*p);
        menuSprites_[i].SetOrigin(Vector2(frames[i].right * 0.5f, frames[i].bottom * 0.5f));
        menuSprites_[i].SetScale(spr_.GetScale());
        menuSprites_[i].GetAnimation().Initialise(i * 2, 1 + (i * 2), 0, false);
        menuSprites_[i].SetPosition(Vector2(spr_.GetScreenSize().x * 0.5f, (spr_.GetScreenSize().y * 0.5f) + (menuSprites_[i].GetScreenSize().y * 1.5f) * i));
        menuSprites_[i].GetAnimation().Play(false);
    }

    //Assign flags
    flagData_.isObjectActive = true;
}

void MainMenuObj::Update(float dTime)
{
    //Handy Handle
    Game& game = Game::Get();

    HandleGPInputs();

    //Check for collision for each menu item, and then when collision has been detected then check if the player clicked a menu item
    for (unsigned int i(0); i < menuSprites_.size(); ++i)
    {
        if (i == menuHoverID_)
        {
            //Enlarge slightly
            menuSprites_[i].SetScale(spr_.GetScale() * 1.2f);
        }
        else
        {
            //Normal Scale Here
            menuSprites_[i].SetScale(spr_.GetScale());
        }
    }
}

void MainMenuObj::Render(float dTime, DirectX::SpriteBatch& batch)
{
    for (auto& m : menuSprites_)
    {
        m.Draw(batch);
    }
}
void MainMenuObj::ValidateID()
{
    //ID Validation Check
    if (menuHoverID_ < 0)
        menuHoverID_ = 0;
    else if (menuHoverID_ > FC::MENU_BUTTON_COUNT - 1)
        menuHoverID_ = FC::MENU_BUTTON_COUNT - 1;
}

void MainMenuObj::HandleGPInputs()
{
    //Handy Handle
    Game& game = Game::Get();
    for (size_t i(0); i < XUSER_MAX_COUNT; ++i)
    {
        if (game.GetGP().IsPressed(i, XINPUT_GAMEPAD_DPAD_DOWN))
            ++menuHoverID_;
        else if (game.GetGP().IsPressed(i, XINPUT_GAMEPAD_DPAD_UP))
            --menuHoverID_;

        //Validate
        ValidateID();

        if (menuHoverID_ == 0 && game.GetGP().IsPressed(i, XINPUT_GAMEPAD_A))
            game.GetModeManager().SwitchMode(PLAY_GAME_DEST);
        else if (menuHoverID_ == 1 && game.GetGP().IsPressed(i, XINPUT_GAMEPAD_A))
            PostQuitMessage(0);
    }
}
