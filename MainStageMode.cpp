#include "MainStageMode.h"       //Owner
#include "GameConstants.h"       //consts namespaces
#include "D3DUtil.h"             //DX specific utils

//Level Objects
#include "PlatformObj.h"     //Stage platforms
#include "Player.h"          //For Player Interactions
#include "Health.h"
#include "LivesCount.h"
#include "HealthFrame.h"

////////////////////
/// Constructors ///    
////////////////////

MainStageMode::MainStageMode()
{
    Init();

    //Debug stuff
    debugTimer_.SetTimeMark(2);
    //Set initial player input delay
    actionDelayTimer_.SetTimeMark(0.5f);
}

MainStageMode::~MainStageMode()
{
    Release();
}



/////////////////
/// Overrides ///    
/////////////////

void MainStageMode::Enter()
{
    //Test code for reentering
    for (auto& a : playerObjects_)
    {
        Player* player = dynamic_cast<Player*>(a);
        player->RespawnPlayer(true);
        player->EnableInput(false);
    }

    actionDelayTimer_.ResetTimer();
}

bool MainStageMode::Exit()
{
    for (auto& a : playerObjects_)
    {
        Player* player = dynamic_cast<Player*>(a);
        if (player->GetPlayerLives() <= 0)
        {
            //Update Score and back out
            if (player->GetID() == 0)
                Game::Get().GetScoreManager().UpdateActivePlayersScoreAtIndex(1, 1);
            else
                Game::Get().GetScoreManager().UpdateActivePlayersScoreAtIndex(0, 1);
        }
    }
    return true;
}

void MainStageMode::Update(float dTime)
{
    //Update game object array
    for (auto& a : gameObjects_)
    {
        if (a->GetIsActive())
            a->Update(dTime);
    }
    for (auto& a : playerObjects_)
    {
        if (a->GetIsActive())
            a->Update(dTime);

        if (actionDelayTimer_.IsTimerPastMarker())
        {
            a->EnableInput(true);
        }
    }
    //Update projectile object array
    for (auto& a : stageObjects_)
    {
        if (a->GetIsActive())
            a->Update(dTime);
    }

    //Update collision
    ManageCollisions();

    //Manage player statuses (lives, OOB etc)
    CheckPlayerStatuses();
    //Debug
    debugTimer_.UpdateTimer(dTime);
    actionDelayTimer_.UpdateTimer(dTime);

    //Health
    HealthUpdate();
}

void MainStageMode::Render(float dTime, DirectX::SpriteBatch& batch)
{
    //Render scene
    for (auto& a : backgroundScene_)
    {
        a.Draw(batch);
    }

    //Render projectiles
    for (auto& a : stageObjects_)
    {
        if (a->GetIsActive())
            a->Render(dTime, batch);
    }

    //Render game objects
    for (auto& a : gameObjects_)
    {
        if (a->GetIsActive())
            a->Render(dTime, batch);
    }

    //Render game objects
    for (auto& a : playerObjects_)
    {
        if (a->GetIsActive())
            a->Render(dTime, batch);
    }

}

void MainStageMode::ProcessKey(char key)
{

}



////////////////////////
/// Public Functions ///    
////////////////////////

iEntity* MainStageMode::GetPlayer(size_t index)
{
    assert(index >= 0 && index <= playerObjects_.size());
    return playerObjects_[index];
}

RECTF& MainStageMode::GetPlayArea()
{
    return outOfBoundsArea_;
}



/////////////////
/// Collision ///
/////////////////

void MainStageMode::ManageCollisions()
{
    //Manage player collision interactions
    PlayerCollisons();
}

void MainStageMode::PlayerCollisons()
{
    //Handy Handle
    Game& game = Game::Get();
    AudioMgrFMOD& audio = AudioMgrFMOD::Get();
    //Capture player 1 and 2 and d cast them
    Player* player1 = dynamic_cast<Player*>(playerObjects_[0]);
    Player* player2 = dynamic_cast<Player*>(playerObjects_[1]);
    Health* bar1 = dynamic_cast<Health*>(gameObjects_[0]);
    Health* bar2 = dynamic_cast<Health*>(gameObjects_[1]);

    if (debugTimer_.IsTimerPastMarker())
    {
        DBOUT("Player 1 Position: " << player1->GetSprite().GetPosition().x << ", " << player1->GetSprite().GetPosition().y);
        DBOUT("Player 2 Position: " << player2->GetSprite().GetPosition().x << ", " << player2->GetSprite().GetPosition().y);
        DBOUT("Player 1 Attack State: " << (int)player1->GetAttackState());
        debugTimer_.ResetTimer();
    }

    //Check Player One Collisions
    for (size_t i(0); i < player1->GetHitboxCount(); ++i)
    {

        if (player1->GetHitboxAtIndex(i)->GetIsActive())
        {
            if (game.GetCollisionManager().Rect2RectCollision(player1->GetHitboxAtIndex(i)->Get2DDefinedBoxCollider(),
                player2->Get2DDefinedBoxCollider()))
            {
                //If player 2 not in the hit list
                if (!player1->GetHitboxAtIndex(i)->IsObjectInList(player2))
                {
                    //Do something to player 2
                    DBOUT("Player 2 Hit!!");
                    audio.GetSfxMgr()->Play("punch1", false, false, nullptr, 0.4f);
                    player1->ProcessAttackDmg(player1, player2);
                    bar2->ProcessHealthBar2(bar2, player2);
                    ManageHitCounterInteraction(player1, player2);
                    player2->HealthDebug();
                    player1->GetHitboxAtIndex(i)->GetHitList().push_back(player2);
                }
            }
        }
    }

    //Check Player Two Collisions
    for (size_t i(0); i < player2->GetHitboxCount(); ++i)
    {

        if (player2->GetHitboxAtIndex(i)->GetIsActive())
        {
            if (game.GetCollisionManager().Rect2RectCollision(player2->GetHitboxAtIndex(i)->Get2DDefinedBoxCollider(),
                player1->Get2DDefinedBoxCollider()))
            {
                //If player 1 is not in list
                if (!player2->GetHitboxAtIndex(i)->IsObjectInList(player1))
                {
                    //Do something to player 1
                    DBOUT("Player 1 Hit!!");
                    audio.GetSfxMgr()->Play("punch2", false, false, nullptr, 0.4f);
                    player2->ProcessAttackDmg(player2, player1);
                    bar1->ProcessHealthBar1(bar1, player1);
                    ManageHitCounterInteraction(player2, player1);
                    player1->HealthDebug();
                    player2->GetHitboxAtIndex(i)->GetHitList().push_back(player1);
                }
            }
        }
    }
}



//////////////
/// Combat ///
//////////////

void MainStageMode::ManageHitCounterInteraction(Player* attPlayer, Player* defPlayer)
{
    assert(attPlayer != defPlayer);

    //Add hit to counter
    defPlayer->AddToHitCounter();
    
    //Get the players hit count and flag if they need to stagger
    if (defPlayer->GetHitCounter() >= 3)
    {
        defPlayer->ProcessStaggerConditions(attPlayer);
    }
}



////////////////////////
/// Condition Checks ///
////////////////////////

void MainStageMode::CheckPlayerStatuses()
{
    //Check the HP value and call death if requirement met
    CheckPlayerHPStatus();
    //Check lives count of each player
    CheckLivesCount();
    //Check if player is out of bounds (offscreen)
    CheckOutOfBounds();
}

void MainStageMode::CheckPlayerHPStatus()
{

}

void MainStageMode::CheckLivesCount()
{
    for (size_t i(0); i < playerObjects_.size(); ++i)
    {
        Player* player = dynamic_cast<Player*>(playerObjects_[i]);
        //If player object is active and has run out of lives
        if (player->GetIsActive() && player->GetPlayerLives() <= 0)
        {
            //Use score system here
            Game::Get().GetModeManager().SwitchMode("POST_GAME");
        }
    }
}

void MainStageMode::CheckOutOfBounds()
{

    for (size_t i(0); i < playerObjects_.size(); ++i)
    {
        Player* player = dynamic_cast<Player*>(playerObjects_[i]);
        //If player alive and out of bounds
        if (player->GetPlayerState() == PLAYER_STATE::ALIVE && IsPlayerOutOfBounds(player))
        {
            //Testing!!!
            player->PlayerDeath();
        }
    }
}

bool MainStageMode::IsPlayerOutOfBounds(Player* player)
{
    Vector2& playerPos = player->GetSprite().GetPosition();
    Vector2& playerSize = player->Get2DCollisionDimensions();
    
    //Check if the player is outside of the screen + additional margin. (LEFT, RIGHT, TOP, BOTTOM)
    if (playerPos.x < 0 - playerSize.x * 2.f || playerPos.x > outOfBoundsArea_.right + playerSize.x * 2.f ||
        playerPos.y < 0 - playerSize.y * 2.f || playerPos.y > outOfBoundsArea_.bottom + playerSize.y * 2.f)
        return true;
    return false;
}



////////////////////
/// Initialising ///
////////////////////

void MainStageMode::Init()
{
    //Vector Memory Reserve
    ReserveMemory();
    //Load players, platforms etc.
    InsertStageObjects();
    //Load background scene
    InitialiseBackground();
    //Configure initial gamestate
    ConfigureGameState();
    
}

void MainStageMode::ReserveMemory()
{
    assert(gameObjects_.empty());
    assert(stageObjects_.empty());
    assert(playerObjects_.empty());

    //Reserve memory
    gameObjects_.reserve(12);
    stageObjects_.reserve(8);
    playerObjects_.reserve(8);

}

void MainStageMode::InsertStageObjects()
{
    //Handy Handles
    WinUtil& win = WinUtil::Get();
    MyD3D& d3d = Game::Get().GetD3D();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    ///////////////
    /// Players ///
    ///////////////

    //Add players to score manager
    Game::Get().GetScoreManager().AddToActivePlayers(2);

    //Init Values
    const size_t PLAYER_COUNT = 2;

    //Create player objects
    for (size_t i(0); i < PLAYER_COUNT; ++i)
    {
        Player* player = new Player(int(i));
        player->SetMode(*this);
        player->SetIsActive(true);
        Add(playerObjects_, player);

    }


    /////////////////////
    /// Stage Objects ///
    /////////////////////

    //Platform values
    const size_t PLATFORM_COUNT = 3;
    const vector<Vector2> POS = { {0.5f, 0.8f}, {0.2f, 0.5f}, {0.8f, 0.5f} };

    //Create platforms (Middle, Left, Right)
    for (size_t i(0); i < PLATFORM_COUNT; ++i)
    {
        PlatformObj* pf = new PlatformObj();
        pf->SetIsActive(true);
        pf->GetSprite().SetTextureRect(FD::redPlatformAtlasFrames[i]);
        pf->GetSprite().SetOrigin(Vector2((FD::redPlatformAtlasFrames[i].right - FD::redPlatformAtlasFrames[i].left) * 0.5f,
            (FD::redPlatformAtlasFrames[i].bottom - FD::redPlatformAtlasFrames[i].top) * 0.5f));
        pf->GetSprite().SetPosition(Vector2(width * POS[i].x, height * POS[i].y));
        pf->GetSprite().SetScale(Vector2(1, 1));
        Add(stageObjects_, pf);
    }

    ///////////////////
    /// UI Elements ///
    ///////////////////

    HealthInit();
}

void MainStageMode::InitialiseBackground()
{
    assert(backgroundScene_.empty());
    //Handy Handle
    Game& game = Game::Get();
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Insert sprite and load texture
    backgroundScene_.insert(backgroundScene_.begin(), FC::MAIN_MENU_BG_COUNT, Sprite(game.GetD3D()));
    ID3D11ShaderResourceView* p = Game::Get().GetD3D().GetCache().LoadTexture(&game.GetD3D().GetDevice()
        , "data/backgrounds/stage_background_1.dds", "Game Background", false);

    //Configure sprite
    backgroundScene_[0].SetTexture(*p);
    backgroundScene_[0].SetScale(Vector2(width / backgroundScene_[0].GetTextureRect().x,
        height / backgroundScene_[0].GetTextureRect().y));

}

void MainStageMode::ConfigureGameState()
{
    WinUtil& win = WinUtil::Get();
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    outOfBoundsArea_.left = 0;
    outOfBoundsArea_.top = 0;
    outOfBoundsArea_.right = (float)width - outOfBoundsArea_.left;
    outOfBoundsArea_.bottom = (float)height;
}

void MainStageMode::HealthUpdate()
{
    Player* player1 = dynamic_cast<Player*>(playerObjects_[0]);
    Player* player2 = dynamic_cast<Player*>(playerObjects_[1]);
    Health* bar1 = dynamic_cast<Health*>(gameObjects_[0]);
    Health* bar2 = dynamic_cast<Health*>(gameObjects_[1]);
    LivesCount* lives1 = dynamic_cast<LivesCount*>(gameObjects_[2]);
    LivesCount* lives2 = dynamic_cast<LivesCount*>(gameObjects_[3]);

    bar1->ProcessHealthBar1(bar1, player1);
    bar2->ProcessHealthBar2(bar2, player2);
    lives1->ProcessLives1(lives1, player1);
    lives2->ProcessLives2(lives2, player2);
}

void MainStageMode::HealthInit()
{
    //Handy Handles
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //HP Bar values
    const size_t HP_BAR_COUNT = 2;
    const vector<Vector2> HP_POS = { {FC::HP_1X, FC::HP_1Y}, {FC::HP_2X, FC::HP_2Y} };
    //Create HP bars (Player 1 Left, Player 2 Right)
    for (size_t i(0); i < HP_BAR_COUNT; ++i)
    {
        Health* hpBar = new Health();
        hpBar->SetIsActive(true);
        hpBar->GetSprite().SetPosition(Vector2(width * HP_POS[i].x, height * HP_POS[i].y));
        Add(gameObjects_, hpBar);
    }

    //Lives count
    const size_t LIVES_COUNT = 2;
    const vector<Vector2> LIVES_POS = { {FC::LC_1X, FC::LC_1Y} , {FC::LC_2X,FC::LC_2Y} };
    //create lives count (player 1 left, player 2 right)
    for (size_t i(0); i < LIVES_COUNT; ++i)
    {
        LivesCount* lifeCount = new LivesCount();
        lifeCount->SetIsActive(true);
        lifeCount->GetSprite().SetPosition(Vector2(width * LIVES_POS[i].x, height * LIVES_POS[i].y));
        Add(gameObjects_, lifeCount);
    }

    //HP frames
    HealthFrame* hpFrame = new HealthFrame();
    hpFrame->SetIsActive(true);
    hpFrame->GetSprite().SetPosition(Vector2(width * FC::HF_1X, height * FC::HF_1Y));
    Add(gameObjects_, hpFrame);

    hpFrame = new HealthFrame();
    hpFrame->SetIsActive(true);
    hpFrame->GetSprite().SetPosition(Vector2(width * FC::HF_2X, height * FC::HF_2Y));
    hpFrame->GetSprite().SetTextureRect(FD::UIAtlasOneFrames[4]);
    hpFrame->GetSprite().SetSpriteEffect(SprEffects::FLIP_HORIZONTAL);
    Add(gameObjects_, hpFrame);
}



/////////////////
/// Utilities ///
/////////////////

void MainStageMode::Add(std::vector<iEntity*>& container, iEntity* object)
{
    assert(object);
    container.push_back(object);
}

void MainStageMode::Remove(std::vector<iEntity*>& container, iEntity* object)
{
    size_t aSize = container.size();
    assert(aSize > 0);
    container.erase(std::remove(container.begin(), container.end(), object));
    assert(aSize != container.size());
    delete object;
}

void MainStageMode::Release()
{
    //Clear up containers
    for (size_t i(0); i < gameObjects_.size(); ++i)
    {
        delete gameObjects_[i];
    }
    for (size_t i(0); i < stageObjects_.size(); ++i)
    {
        delete stageObjects_[i];
    }
    for (size_t i(0); i < playerObjects_.size(); ++i)
    {
        delete playerObjects_[i];
    }

    if (font1_ != nullptr)
        delete font1_;


    //Clear containers
    gameObjects_.clear();
    stageObjects_.clear();
    backgroundScene_.clear();
}
