#pragma once

#include <vector>            //Vector array

#include "ModeInterface.h"   //Mode Parent
#include "Sprite.h"          //For making sprite vector arrays
#include "EntityInterface.h" //For making entity vector arrays
#include "Game.h"            //Grab D3D reference for sprites from here
#include "WindowUtils.h"        

/*
Splash mode, any splash art, logos etc would go here. Proceeds into Intro Mode on a timer or with a prompt
*/

class SplashMode : public iMode
{
public:
    //Handle for changing the modes
    inline static const std::string MODE_NAME = "SPLASH";

    SplashMode();
    ~SplashMode();

    //Parent overrides
    void Enter() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    std::string GetModeName() const override { return MODE_NAME; }

private:

    //Swap the mode on a timer
    float modeSwapTimer_ = 0;

    //Hold any splash sprites in here
    std::vector<Sprite> splashes_;

    //Initialise block
    void InitBackground();
    //Release resources
    void Release();

};