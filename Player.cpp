#include "Player.h"

////////////////////
/// Constructors ///    
////////////////////

Player::Player()
    : iEntity(Game::Get().GetD3D())
{
    Initialise();
}

Player::Player(int playerID)
    : iEntity(Game::Get().GetD3D()), playerID_(playerID)
{
    Initialise();
}



/////////////////
/// Overrides ///    
/////////////////

void Player::Update(float dTime)
{
    //PreState check to see if the player should be dead
    if (GetHealthSystem().health <= 0 && playerState_ == PLAYER_STATE::ALIVE)
        PlayerDeath();

    switch (playerState_)
    {
    case(PLAYER_STATE::ALIVE):
        //Reset frame specific flags
        PreFrameFlagReset();
        //Preflag check
        PreUpdateFlagCheck();
        //Update Input Flags
        GetInputEvents();
        //Update Sprite with Keyboard
        UpdateMovement(dTime);
        //Update the sprite animation
        UpdateAnimation(dTime);
        //Update collision logic
        UpdateCollision();
        //Post update flag check
        PostFlagCheck();
        //Update any logic independant timers
        UpdateTimers(dTime);
        break;
    case(PLAYER_STATE::DEAD):
        //Update Spawn Timer
        respawnTimer_.UpdateTimer(dTime);
        if (respawnTimer_.IsTimerPastMarker())
            playerState_ = PLAYER_STATE::RESPAWNING;
        break;
    case(PLAYER_STATE::RESPAWNING):
        RespawnPlayer(false);
        break;
    }

}

void Player::Render(float dTime, DirectX::SpriteBatch& batch)
{
    if(playerState_ == PLAYER_STATE::ALIVE)
        iEntity::Render(dTime, batch);

    /*
        Revisit this for respawning effect
    */

    //if (playerState_ == PLAYER_STATE::RESPAWNING && staggerFlags_.flag8)
    //{
    //    //Using a spare flag here to flash every other frame
    //    staggerFlags_.flag8 = false;
    //    iEntity::Render(dTime, batch);
    //}
    //else //Skip a frame
    //    staggerFlags_.flag8 = true;

}

void Player::ResetVariables()
{
    //When restarting stage do stuff here.
}



////////////////////////
/// Public Functions ///    
////////////////////////

void Player::AddToHitCounter()
{
    //Add new hit
    ++hits_;
    DBOUT("Hit Count: " << hits_);
    //Restart the clock
    hitBuildUpWindow_.ResetTimer();
}

RectHitbox* Player::GetHitboxAtIndex(size_t index)
{
    assert(index >= 0 && index <= hitboxes_.size());
    return &hitboxes_[index];
}

void Player::ProcessAttackDmg(Player* attackingPlayer, Player* defendingPlayer)
{
    switch (attackingPlayer->attackState_)
    {
    case(ATTACK_STATE::STATIC_ATT_LEFT):
        defendingPlayer->healthSystem.damageDealt = GC::P_ATTACK1_DMG * defendingPlayer->healthSystem.damageMultiplier;
        defendingPlayer->healthSystem.health -= defendingPlayer->healthSystem.damageDealt;
        break;
    case(ATTACK_STATE::STATIC_ATT_RIGHT):
        defendingPlayer->healthSystem.damageDealt = GC::P_ATTACK1_DMG * defendingPlayer->healthSystem.damageMultiplier;
        defendingPlayer->healthSystem.health -= defendingPlayer->healthSystem.damageDealt;
        break;
    case(ATTACK_STATE::MOVE_ATT_LEFT):
        defendingPlayer->healthSystem.damageDealt = GC::P_ATTACK2_DMG * defendingPlayer->healthSystem.damageMultiplier;
        defendingPlayer->healthSystem.health -= defendingPlayer->healthSystem.damageDealt;
        break;
    case(ATTACK_STATE::MOVE_ATT_RIGHT):
        defendingPlayer->healthSystem.damageDealt = GC::P_ATTACK2_DMG * defendingPlayer->healthSystem.damageMultiplier;
        defendingPlayer->healthSystem.health -= defendingPlayer->healthSystem.damageDealt;
        break;
    case(ATTACK_STATE::JUMPING_ATT):
        defendingPlayer->healthSystem.damageDealt = GC::P_ATTACK3_DMG * defendingPlayer->healthSystem.damageMultiplier;
        defendingPlayer->healthSystem.health -= defendingPlayer->healthSystem.damageDealt;
        break;
    case(ATTACK_STATE::FALLING_ATT):
        defendingPlayer->healthSystem.damageDealt = GC::P_ATTACK3_DMG * defendingPlayer->healthSystem.damageMultiplier;
        defendingPlayer->healthSystem.health -= defendingPlayer->healthSystem.damageDealt;
        defendingPlayer->AddToHitCounter();
        defendingPlayer->AddToHitCounter();
        break;
    }

}

void Player::ProcessStaggerConditions(Player* attPlayer)
{
    //Using attacking player position, flag stagger direction
    if (attPlayer->GetSprite().GetPosition().x >= this->spr_.GetPosition().x)
        staggerFlags_.flag2 = true;    //Stagger Left
    else
        staggerFlags_.flag3 = true;    //Stagger Right

    attackState_ = ATTACK_STATE::NOT_ATTACKING;
    mdFlags_.isAttacking = false;
}

void Player::PlayerDeath()
{
    //Subtract 1 from total death count
    playerLives_--;
    //Set live to zero for visuals (They fell of the screen)
    healthSystem.health = 0;
    //Set to dead
    playerState_ = PLAYER_STATE::DEAD;
}


////////////////////////
/// Control/Movement ///
////////////////////////

void Player::PreFrameFlagReset()
{
    //Reset certain flags
    mdFlags_.movingLeft = false;
    mdFlags_.movingRight = false;
}

void Player::GetInputEvents()
{
    if (flagData_.enableInputs)
    {
        //Handy Handles
        Game& game = Game::Get();

        //Directional Flags
        inputFlags_.isUpPressed = false;
        inputFlags_.isDownPressed = false;
        inputFlags_.isLeftPressed = false;
        inputFlags_.isRightPressed = false;

        //Jump Flag
        inputFlags_.isAction1Pressed = false;

        //Attack Flag
        inputFlags_.isAction2Pressed = false;

        //Check gamepad inputs
        if (game.GetGP().IsConnected(playerID_))
        {
            inputFlags_.isLeftStickMoved = game.GetGP().GetState(playerID_).leftStickX != 0;

            //If we didn't positive before, check again
            if (!inputFlags_.isAction1Pressed)
                inputFlags_.isAction1Pressed = game.GetGP().IsPressed(playerID_, XINPUT_GAMEPAD_A);
            if (!inputFlags_.isAction2Pressed)
                inputFlags_.isAction2Pressed = game.GetGP().IsPressed(playerID_, XINPUT_GAMEPAD_B);
        }
    }
}

void Player::UpdateMovement(float dTime)
{
    //Handy Handles
    Game& game = Game::Get();

    //Capture any position changes here
    Vector2 playerPos(0, 0);
    //Capture original position for comparison later
    Vector2 oldPos = spr_.GetPosition();

    //Check for any keyboard inputs for player movement
    bool isMovementKeyPressed = inputFlags_.isUpPressed || inputFlags_.isDownPressed
        || inputFlags_.isLeftPressed || inputFlags_.isRightPressed || inputFlags_.isAction1Pressed || inputFlags_.isLeftStickMoved;


    //Update actions for frame
    UpdateActions(dTime);

    //Check if the player is under a status that prevents their own controlled movement
    if(UpdateStatusMovement(dTime, playerPos))
    { }
    else if(UpdateHorizontalMovement(dTime, playerPos, isMovementKeyPressed))
    { }
    //Update vertial movement (jumping, falling)
    UpdateVerticalMovement(dTime, playerPos);

    //Add current player position to new position
    playerPos += spr_.GetPosition();
    //Then set this as the new player position
    spr_.SetPosition(playerPos);

    //Validate Position against stage platforms
    ValidatePositionPlatforms();
    //////Validate Position against window edges
    //ValidatePositionWindow();

    //If old pos is == to new pos, flag not moving
    if (oldPos == spr_.GetPosition())
        mdFlags_.isStationary = true;
    else
        mdFlags_.isStationary = false;
}

void Player::UpdateGamepad(float dTime)
{

}

bool Player::UpdateStatusMovement(float dTime, Vector2& playerPos)
{
    //If we are flagged to stagger
    
    /*
        Update this some where to be knocked back relative to the opposing player at the time of
        the knockback
    */
    if (staggerFlags_.flag1)
    {
        //Stagger Left
        if (staggerFlags_.flag2)
        {
            playerPos.x -= GC::P_KNOCK_BACK_SPEED * adj_.spdAdj.x * dTime;
        }
        //Stagger Right
        else if (staggerFlags_.flag3)
        {
            playerPos.x += GC::P_KNOCK_BACK_SPEED * adj_.spdAdj.x * dTime;
        }
        return true;
    }
    return false;
}

bool Player::UpdateHorizontalMovement(float dTime, Vector2& playerPos, bool isMovePressed)
{
    //Handy Handles
    Game& game = Game::Get();

    //If action flagged and player not attacking
    if (isMovePressed && !mdFlags_.isAttacking)
    {
        //Check if stick moved
        if (inputFlags_.isLeftStickMoved)
        {
            //Capture old x position
            float posX = spr_.GetPosition().x;
            if (mdFlags_.isJumping || mdFlags_.isFalling)
                playerPos.x += game.GetGP().GetState(playerID_).leftStickX * 600 * adj_.spdAdj.x * dTime;
            else
                playerPos.x += game.GetGP().GetState(playerID_).leftStickX * GC::P_MOVE_SPEED * adj_.spdAdj.x * dTime;

            //Flag anim
            if (playerPos.x + posX < posX)
                mdFlags_.movingLeft = true;
            else if (playerPos.x + posX > posX)
                mdFlags_.movingRight = true;
        }
        //else check for keyboard
        else
        {
            if (inputFlags_.isLeftPressed)            //Left
            {

                if (mdFlags_.isJumping || mdFlags_.isFalling)
                    playerPos.x -= 600 * adj_.spdAdj.x * dTime;
                else
                    playerPos.x -= GC::P_MOVE_SPEED * adj_.spdAdj.x * dTime;
                mdFlags_.movingLeft = true;
            }
            else if (inputFlags_.isRightPressed)      //Right
            {
                if (mdFlags_.isJumping || mdFlags_.isFalling)
                    playerPos.x += 600 * adj_.spdAdj.x * dTime;
                else
                    playerPos.x += GC::P_MOVE_SPEED * adj_.spdAdj.x * dTime;
                mdFlags_.movingRight = true;
            }
        }
        return true;
    }

    return false;
}

void Player::UpdateVerticalMovement(float dTime, Vector2& playerPos)
{

    //If currently jumping or falling and adjust position accordingly
    if (mdFlags_.isJumping)
    {
        playerPos.y -= GC::P_JMP_SPEED * adj_.spdAdj.y * dTime;    
    }

    if (attackState_ == ATTACK_STATE::FALLING_ATT)
    {
        //Update player pos using gravity
        playerPos.y += 800 * adj_.spdAdj.y * dTime;
    }
    else
    {
        //Update player pos using gravity
        playerPos.y += GC::G_GRAVITY * adj_.spdAdj.y * dTime;
    }

}

void Player::UpdateActions(float dTime)
{
    //Check if the player asked to jump, and if allowed to jump
    if (inputFlags_.isAction1Pressed && !mdFlags_.isJumping && !mdFlags_.isFalling)
    {
        //Flag now jumping
        mdFlags_.isJumping = true;
        mdFlags_.isFalling = false;

        jumpDuration_.ResetTimer();
    }
    //Flag attacking on ground, and able to attack (not staggered)
    if (inputFlags_.isAction2Pressed && !mdFlags_.isJumping && !mdFlags_.isFalling && !staggerFlags_.flag1)
    {
        mdFlags_.isAttacking = true;
        attackState_ = ATTACK_STATE::GROUND_ATTACK;
    }
    else if (inputFlags_.isAction2Pressed && !staggerFlags_.flag1 && (mdFlags_.isFalling || mdFlags_.isJumping) &&
        jumpDuration_.GetTimer() >= 0.2f)
    {
        mdFlags_.isAttacking = true;
        mdFlags_.isFalling = true;
        mdFlags_.isJumping = false;
        attackState_ = ATTACK_STATE::FALLING_ATT;
    }
}

void Player::ValidatePositionPlatforms()
{
    //Handy handle
    Game& game = Game::Get();

    std::vector<iEntity*>& terrain = ownerMode_->GetStageObjects();

    for (size_t i(0); i < terrain.size(); ++i)
    {
        //If the player collides with object.
        if (game.GetCollisionManager().Rect2RectCollision(terrain[i]->Get2DDefinedBoxCollider()
            , Get2DDefinedBoxCollider()))
        {
            //Capture player values
            Vector2& plPos = spr_.GetPosition();
            Vector2& plHSize = colData_.collisionBox2D * 0.5f;
            //Capture terrain values
            Vector2& tPos = terrain[i]->GetSprite().GetPosition();
            Vector2& tHSize = terrain[i]->Get2DCollisionDimensions() * 0.5f;


            //Check against top
            if ((plPos.y < tPos.y - tHSize.y) &&
                (plPos.x >= tPos.x - tHSize.x - (plHSize.x * 0.5f) &&
                plPos.x <= tPos.x + tHSize.x + (plHSize.x * 0.5f)))
            {
                plPos.y = tPos.y - tHSize.y - plHSize.y;
                mdFlags_.isFalling = false;
            }
            //Check against bottom
            else if (plPos.y > tPos.y + tHSize.y &&
                (plPos.x >= tPos.x - tHSize.x - (plHSize.x * 0.5f) &&
                    plPos.x <= tPos.x + tHSize.x + (plHSize.x * 0.5f)))
            {
                plPos.y = tPos.y + tHSize.y + plHSize.y;
                jumpDuration_.SetTimerEqualToMarker();
            }
            //Check against left side of object
            if ((!(plPos.x >= tPos.x - tHSize.x && plPos.x <= tPos.x + tHSize.x) &&
                (plPos.x + plHSize.x >= tPos.x - tHSize.x && plPos.x < tPos.x) &&
                (plPos.y + plHSize.y > tPos.y - tHSize.y)))
            {
                plPos.x = tPos.x - tHSize.x - plHSize.x;
            }
            //Check against right side of object
            else if ((!(plPos.x >= tPos.x - tHSize.x && plPos.x <= tPos.x + tHSize.x) &&
                (plPos.x - plHSize.x <= tPos.x + tHSize.x && plPos.x > tPos.x) &&
                (plPos.y + plHSize.y > tPos.y - tHSize.y)))
            {
                plPos.x = tPos.x + tHSize.x + plHSize.x;
            }

            //Set position
            spr_.SetPosition(plPos);

            break;
        }
    }
}

void Player::ValidatePositionWindow()
{
    //Capture player position
    Vector2 pos = spr_.GetPosition();

    //Define playarea (like the screen)
    RECTF playArea = ownerMode_->GetPlayArea();

    Vector2 halfSize = colData_.collisionBox2D * 0.5f;

    //Check player position against our play area limitations
    if (pos.x - halfSize.x < playArea.left)         //Left side
        pos.x = playArea.left + halfSize.x;
    else if (pos.x + halfSize.x > playArea.right)   //Right
        pos.x = playArea.right - halfSize.x;
    if (pos.y - halfSize.y < playArea.top)          //Top
    {
        pos.y = playArea.top + halfSize.y;
        //The player hit something solid, time to end the jump
        jumpDuration_.SetTimerEqualToMarker();
    }
    else if (pos.y + halfSize.y > playArea.bottom)  //Bottom
    {
        pos.y = playArea.bottom - halfSize.y;
        //Flag that that player is on solid ground
        mdFlags_.isFalling = false;
    }

    //Set new position
    spr_.SetPosition(pos);
}



//////////////////
/// Animations ///    
//////////////////

void Player::UpdateAnimation(float dTime)
{
    //Check if we are attacking and update animation accordingly
    if (UpdateStatusAnimations())
    { }
    else if (UpdateAttackAnimations())
    { }
    else //Else determine what animation to play based on flags
        UpdateMovementAnimations();

    //Check what direction the player needs to be facing
    UpdateFacingDirection();

    //Update animation 
    spr_.GetAnimation().Update(dTime);

    //Check what origin needs to be active based on animation frame
    ValidateOrigin();
}

bool Player::UpdateStatusAnimations()
{
    if (staggerFlags_.flag1)
    {
        if (spr_.GetAnimation().GetCurrentAnimation()->animName != "Stagger")
            spr_.GetAnimation().SetAnimation(5);
        else if (spr_.GetAnimation().GetCurrentAnimation()->isAnimActive == false)
            spr_.GetAnimation().SetAnimation(5);

        return true;
    }

    return false;
}

void Player::UpdateMovementAnimations()
{
    //If the player is doing nothing, and flagged to do nothing
    if (mdFlags_.isStationary && !mdFlags_.isFalling && !mdFlags_.isJumping)
    {
        if (spr_.GetAnimation().GetCurrentAnimation()->animName != "Idle")
            spr_.GetAnimation().SetAnimation(0);
        else if (spr_.GetAnimation().GetCurrentAnimation()->isAnimActive == false)
            spr_.GetAnimation().SetAnimation(0);
    }
    //If the player is just moving left/right
    else if ((mdFlags_.movingLeft || mdFlags_.movingRight) && !mdFlags_.isJumping && !mdFlags_.isFalling)
    {
        if (spr_.GetAnimation().GetCurrentAnimation()->animName != "Run")

            spr_.GetAnimation().SetAnimation(1);
        else if (spr_.GetAnimation().GetCurrentAnimation()->isAnimActive == false)
            spr_.GetAnimation().SetAnimation(1);
    }
    //If the player is jumping
    else if (mdFlags_.isJumping)
    {
        if (spr_.GetAnimation().GetCurrentAnimation()->animName != "Jump")
            spr_.GetAnimation().SetAnimation(2, true, false);
        else if (spr_.GetAnimation().GetCurrentAnimation()->isAnimActive == false)
            spr_.GetAnimation().SetAnimation(2);

    }
}

bool Player::UpdateAttackAnimations()
{
    //If the player is attacking
    if (mdFlags_.isAttacking)
    {
        switch (attackState_)
        {
        case(ATTACK_STATE::GROUND_ATTACK):
            if (spr_.GetAnimation().GetCurrentAnimation()->animName != "Punch Attack")
                spr_.GetAnimation().SetAnimation(3, true, false);
            else if (spr_.GetAnimation().GetCurrentAnimation()->isAnimActive == false)
                spr_.GetAnimation().SetAnimation(3, true, false);
            return true;
        case(ATTACK_STATE::FALLING_ATT):
            if (spr_.GetAnimation().GetCurrentAnimation()->animName != "Drop Attack")
                spr_.GetAnimation().SetAnimation(4, true, true);
            else if (spr_.GetAnimation().GetCurrentAnimation()->isAnimActive == false)
                spr_.GetAnimation().SetAnimation(4, true, true);
        }

        return true;
    }

    return false;
}

void Player::UpdateFacingDirection()
{
    if (!mdFlags_.isAttacking)
    {
        if (mdFlags_.movingLeft || staggerFlags_.flag3)
        {
            spr_.SetSpriteEffect(SprEffects::FLIP_HORIZONTAL);
            mdFlags_.isLookingLeft = true;
            mdFlags_.isLookingRight = false;
        }
        else if (mdFlags_.movingRight || staggerFlags_.flag2)
        {
            spr_.SetSpriteEffect(SprEffects::NONE);
            mdFlags_.isLookingLeft = false;
            mdFlags_.isLookingRight = true;
        }
    }

}

void Player::ValidateOrigin()
{
    //Check what frame is active to determine if switching the origin is required.

}



//////////////
/// Combat ///
//////////////

void Player::UpdateCollision()
{
    //Disable all hitboxes to prevent lingering hitboxes
    PreCollisionCleanUp();

    //Flag if hitboxes can be activated based on frame
    HitboxFlagCheck();

    if (mdFlags_.isAttacking)
        ProcessAttack();

    CheckHitboxLists();
}

void Player::PreCollisionCleanUp()
{
    for (auto& a : hitboxes_)
        a.DisableHitbox(false);

    memset(&canDamageFlags_, 0, sizeof(canDamageFlags_));
}

void Player::ProcessAttack()
{

    //Check against flags and set the approprate state to determine type of attack to activate later

    //Standing still attack
    if (mdFlags_.isStationary)
    {
        //Determine what direction is the player facing
        if (mdFlags_.isLookingLeft)
            attackState_ = ATTACK_STATE::STATIC_ATT_LEFT;
        else if (mdFlags_.isLookingRight)
            attackState_ = ATTACK_STATE::STATIC_ATT_RIGHT;
    }
    //Falling attack
    else if (mdFlags_.isFalling || mdFlags_.isJumping) // Do Down Attack
        attackState_ = ATTACK_STATE::FALLING_ATT;

    //Now using the set state to position the collsion box for the attack if conditions met.
    PositionCollisionBox();

}

void Player::HitboxFlagCheck()
{
    //Determine based on frame ID if hitbox should be active or not
    switch (spr_.GetAnimation().GetCurrentFrame())
    {
    //Ground based attack
    case(49):
    case(52):
        if (mdFlags_.isLookingLeft)
            canDamageFlags_.flag1 = true;
        else
            canDamageFlags_.flag2 = true;
        break;

    case(80):
    case(81):
    case(82):
    case(83):
        canDamageFlags_.flag3 = true;
        break;
    }
}

void Player::PositionCollisionBox()
{
    //Capture player pos
    Vector2 pos = spr_.GetPosition();
    //Get player hitbox
    Vector4 size = Get2DDefinedBoxCollider();

    //Determine what attack to enable collision for based on state
    switch (attackState_)
    {
    case(ATTACK_STATE::STATIC_ATT_LEFT):
        if (canDamageFlags_.flag1)
        {
            hitboxes_[0].GetSprite().SetPosition(Vector2(pos.x - size.z * 0.7f, pos.y - size.w * 0.25f));
            hitboxes_[0].SetIsActive(true);
        }
        break;
    case(ATTACK_STATE::STATIC_ATT_RIGHT):
        if (canDamageFlags_.flag2)
        {
            hitboxes_[0].GetSprite().SetPosition(Vector2(pos.x + size.z * 0.7f, pos.y - size.w * 0.25f));
            hitboxes_[0].SetIsActive(true);
        }
        break;
    case(ATTACK_STATE::MOVE_ATT_LEFT):
        DBOUT("");
        break;
    case(ATTACK_STATE::MOVE_ATT_RIGHT):
        DBOUT("");
        break;
    case(ATTACK_STATE::JUMPING_ATT):
        DBOUT("");
        break;
    case(ATTACK_STATE::FALLING_ATT):
        if (canDamageFlags_.flag3)
        {
            hitboxes_[1].GetSprite().SetPosition(Vector2(pos.x, pos.y + (size.w * 0.25f)));
            hitboxes_[1].SetIsActive(true);
        }
        break;
    }

}

void Player::CheckHitboxLists()
{
    for (size_t i(0); i < hitboxes_.size(); ++i)
    {
        //If the table is empty do nothing
        if (hitboxes_[i].GetHitList().empty())
        {
        }
        else if (!hitboxes_[i].GetIsActive()) //If not empty, check if inactive and clear the list if inactive
        {
            hitboxes_[i].GetHitList().clear();
        }
    }
}



////////////
/// Misc ///
////////////

void Player::PreUpdateFlagCheck()
{
    if (hits_ >= 3)
    {
        hits_ = 0;
        hitBuildUpWindow_.ResetTimer();
        staggerDuration_.ResetTimer();
        staggerFlags_.flag1 = true;
    }
}

void Player::UpdateTimers(float dTime)
{
    if (hits_ > 0)
        hitBuildUpWindow_.UpdateTimer(dTime);
    if (staggerFlags_.flag1)
        staggerDuration_.UpdateTimer(dTime);
    
    jumpDuration_.UpdateTimer(dTime);
}

void Player::PostFlagCheck()
{
    //Check the frame and flag not attack if = last frame
    if (spr_.GetAnimation().GetCurrentFrame() == 54)
    {
        mdFlags_.isAttacking = false;
        attackState_ = ATTACK_STATE::NOT_ATTACKING;
    }
    else if ((spr_.GetAnimation().GetCurrentFrame() >= 80 && spr_.GetAnimation().GetCurrentFrame() <= 83) && !mdFlags_.isFalling)
    {
        mdFlags_.isAttacking = false;
        attackState_ = ATTACK_STATE::NOT_ATTACKING;
    }
    //Check if the player is still staggered
    if (staggerDuration_.IsTimerPastMarker() && staggerFlags_.flag1)
    {
        staggerFlags_.flag1 = false;
        staggerFlags_.flag2 = false;
        staggerFlags_.flag3 = false;
    }
    //Check if we are jumping and that its time to fall
    if (mdFlags_.isJumping && jumpDuration_.IsTimerPastMarker())
    {
        //Set flags
        mdFlags_.isJumping = false;
        mdFlags_.isFalling = true;
    }
    //Check if the hit accumulation window is maxed out.
    if (hitBuildUpWindow_.IsTimerPastMarker())
    {
        hits_ = 0;
        hitBuildUpWindow_.ResetTimer();
    }
}



////////////////////
/// Initialising ///
////////////////////

void Player::Initialise()
{
    //Set player texture
    SetTexture();
    //Update scaling values to current screen size
    UpdateScalingAdjustments();
    //Load hitboxes
    SetupHitboxes();
    //Setup Timers
    ConfigureTimers();
}

void Player::SetTexture()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();

    ////Get frame data
    //std::vector<RECTF> frames(castlevaniaTestSprite,
    //    castlevaniaTestSprite + sizeof(castlevaniaTestSprite) / sizeof(castlevaniaTestSprite[0]));

    const float FRAME_SIZE = 128.f;
    std::vector<RECTF> frames;
    frames.insert(frames.begin(), 16 * 16, RECTF());

    int count = 0;
    for (int y(0); y < 16; ++y)
    {
        for (int x(0); x < 16; ++x)
        {
            frames[count++] = RECTF{ x * FRAME_SIZE, y * FRAME_SIZE,
                x * FRAME_SIZE + FRAME_SIZE, y * FRAME_SIZE + FRAME_SIZE };
        }
    }

    //Load the Texture
    ID3D11ShaderResourceView* p = nullptr;
    if(playerID_ == 1)
        p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), PLAYER_FP_BLUE,
            "Player Atlas Blue", false, &frames);
    else
        p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), PLAYER_FP_RED,
            "Player Atlas Red", false, &frames);

    //Set object parameters
    spr_.SetTexture(*p);

    //Run initial player setup
    InitialiseDefaultPlayer(frames);

    p = nullptr;
}

void Player::ConfigureTimers()
{
    //Setup time markers for individual timers
    hitBuildUpWindow_.SetTimeMark(GC::P_STAGGER_WINDOW);
    staggerDuration_.SetTimeMark(GC::P_STAGGER_DURATION);
    jumpDuration_.SetTimeMark(GC::P_JMP_DURATION);
    respawnTimer_.SetTimeMark(GC::P_RESPAWN_TIME);
}

void Player::InitialiseDefaultPlayer(std::vector<RECTF>& frames)
{
    //Handy Handle
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Setup origins to specific values
    //Default Origin
    originData_.defaultOrigin = Vector2((frames[64].right - frames[64].left) * 0.5f,
        (frames[64].bottom - frames[64].top) * 0.5f);

    //Set sprite parameters
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 0.95f, adj_.scaleAdj.y * 0.95f));
    spr_.SetPosition(Vector2(width / 2.f, height / 2.f));
    spr_.SetOrigin(originData_.defaultOrigin);

    //Setup 2D collision box
    colData_.collisionBox2D = Vector2(FC::defCollision.x * spr_.GetScale().x, FC::defCollision.y * spr_.GetScale().y);

    //Setup animation data
    spr_.GetAnimation().AddNewAnimation(64, 64, 2, "Idle");          //0
    spr_.GetAnimation().AddNewAnimation(0, 11, 9, "Run");            //1
    spr_.GetAnimation().AddNewAnimation(32, 35, 9, "Jump");          //2
    spr_.GetAnimation().AddNewAnimation(48, 54, 10, "Punch Attack"); //3
    spr_.GetAnimation().AddNewAnimation(80, 83, 10, "Drop Attack");  //4
    spr_.GetAnimation().AddNewAnimation(16, 16, 10, "Stagger");      //5

    //Set initial anim
    spr_.GetAnimation().SetAnimation(0);
}

void Player::SetupHitboxes()
{
    //Insert the amount of hitboxes into array
    hitboxes_.insert(hitboxes_.begin(), 4, RectHitbox());

    std::vector<Vector2> boxes(attackHitboxes,
        attackHitboxes + sizeof(attackHitboxes) / sizeof(attackHitboxes[0]));

    for (size_t i(0); i < hitboxes_.size(); ++i)
    {
        //Set collision values for hitbox
        hitboxes_[i].Set2DCollisionDimensions(Vector2(boxes[i].x * adj_.scaleAdj.x, boxes[i].y * adj_.scaleAdj.y));
        //Set ghost texture rect based on collision box
        hitboxes_[i].GetSprite().SetTextureRect({ 0, 0, boxes[i].x, boxes[i].y });
        //Center origin
        hitboxes_[i].GetSprite().SetOrigin(Vector2(boxes[i].x * 0.5f, boxes[i].y * 0.5f));
        //Scale
        hitboxes_[i].GetSprite().SetScale(Vector2(1 * adj_.scaleAdj.x, 1 * adj_.scaleAdj.y));
        //Set inactive
        hitboxes_[i].SetIsActive(false);
    }
}

void Player::RespawnPlayer(bool freshGame)
{
    //Handy Handle
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Set all initial variables to normals
    healthSystem.health = GC::P_STARTING_HP;
    hits_ = 0;

    //Set respawn based on ID
    switch (playerID_)
    {
    case(0):
        //Set position
        spr_.SetPosition(Vector2(width * 0.2f, height * 0.15f));

        break;
    case(1):
        spr_.SetPosition(Vector2(width * 0.8f, height * 0.15f));
        spr_.SetSpriteEffect(SprEffects::FLIP_HORIZONTAL);
        break;
    }

    //Set flags
    flagData_.isObjectActive = true;
    flagData_.enableInputs = true;
    mdFlags_.isAttacking = false;
    mdFlags_.isJumping = false;
    mdFlags_.isFalling = true;
    memset(&staggerFlags_, 0, sizeof(&staggerFlags_));
    
    //Set states
    playerState_ = PLAYER_STATE::ALIVE;
    attackState_ = ATTACK_STATE::NOT_ATTACKING;
    moveState_ = MOVEMENT_STATE::STATIONARY;

    //Reset respawn timer
    respawnTimer_.ResetTimer();

    //Set to neutral frame
    spr_.GetAnimation().SetAnimation(0);

    //If its a fresh game then reset starting lives
    if(freshGame)
        playerLives_ = GC::P_STARTING_LIVES;
}
