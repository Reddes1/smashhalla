#pragma once
#include "EntityInterface.h"        //Object Parent
#include "Sprite.h"                 //For sprites
#include "Player.h"

//Health Display Frames
const RECTF healthFrames[]
{
	{70,755,460,1000}, //Hp frame 1
	//{100,517,447,740}, //Hp frame 2
	{756,562,850,572}, //Life count bar
	{740,592,966,600}, //Hp bar
};

class HealthDislpay : public iEntity
{
public:
	HealthDislpay();
	~HealthDislpay();

	void Update(float dTime) override;
	void Render(float dTime, DirectX::SpriteBatch& batch) override;

	//Process Hp and life count bars
	void ProcessHelathBar1(HealthDislpay* bar1, Player* player1);
	void ProcessHealthBar2(HealthDislpay* bar2, Player* player2);
	void ProcessLifeCount1(HealthDislpay* life1, Player* player1);
	void ProcessLifeCount2(HealthDislpay* life2, Player* player2);
	std::vector<Sprite> healthDisplay_;
private:
	

	//setup
	void Initialise();
	void HealthFrameSetup();
};

