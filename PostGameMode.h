#pragma once

#include "Game.h"            //Game Manager
#include "ModeInterface.h"   //Mode Parent
#include "Sprite.h"          //For making sprite vector arrays
#include "SpriteFont.h"      //For screen text

#include "Timer.h"           //Timer Util


/*
    Basic Game Over state that bridges the active mode to a highscore state.
*/
class PostGameMode : public iMode
{
public:
    //HMode Handle
    inline static const std::string MODE_NAME = "POST_GAME";

    PostGameMode();
    ~PostGameMode();

    //Parent overrides
    void Enter() override;
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ProcessKey(char key) override;
    void Initialise();
    std::string GetModeName() const override { return MODE_NAME; }


private:
    //background scene
    std::vector<Sprite> backgrounds_;

    //Fonts
    DirectX::SpriteFont* font1_;    //Algerian

    //timer container
    Timer textDelayTimer_;

    //Initialise block
    void InitialiseBackground();
    //Render any screen text
    void RenderText(DirectX::SpriteBatch& batch);
    //Release resources
    void Release();
};