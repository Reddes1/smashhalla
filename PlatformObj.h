#pragma once
#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class

/*
    TESTING!! Basic platform object. 
*/
class PlatformObj : public iEntity
{
public:

    PlatformObj();
    ~PlatformObj()
    {}

    //Parent overrides
    void Update(float dTime) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;
    void ResetVariables() override;

    //Hold accurate positions of left, top, right, bottom faces.
    RECTF sidesData_;

private:

    void CalculateSidesData();

    //Initialise block
    void Initialise();
};