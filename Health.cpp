#include "Health.h"

Health::Health()
    :iEntity(Game::Get().GetD3D())
{
    Initialise();
}

Health::~Health()
{
}

void Health::Update(float dTIme)
{
}

void Health::Render(float dTime, DirectX::SpriteBatch& batch)
{
    iEntity::Render(dTime, batch);
}

void Health::ProcessHealthBar1(Health* bar1, Player* player1)
{
    bar1->spr_.SetScale(Vector2((adj_.scaleAdj.x * 1.f) * (player1->GetHealthSystem().health / 100), adj_.scaleAdj.y * 1.f));
}

void Health::ProcessHealthBar2(Health* bar2, Player* player2)
{
    bar2->spr_.SetScale(Vector2((adj_.scaleAdj.x * 1.f) * (player2->GetHealthSystem().health / 100), adj_.scaleAdj.y * 1.f));
}



void Health::Initialise()
{
    HealthBarInit();
}

void Health::HealthBarInit()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Update scaling values to current screen size
    UpdateScalingAdjustments();

    //Load texture and frames
    std::vector<RECTF> frames(FD::UIAtlasOneFrames, FD::UIAtlasOneFrames + sizeof(FD::UIAtlasOneFrames) / sizeof(FD::UIAtlasOneFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/UI/UI_Elements2.dds",
        "UI Atlas", false, &frames);
    const Vector2 SIZE = { 226,8 };

    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[5]);
    spr_.SetOrigin(Vector2(SIZE.x / 2, SIZE.y / 2));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 1.f, adj_.scaleAdj.y * 1.f));
    spr_.SetPosition(Vector2(width * 0.1f, height * 0.1f));
}
