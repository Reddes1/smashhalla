#include <vector>
#include <string>

#include "ModeInterface.h"

#include "D3D.h"
#include "SpriteBatch.h"

#pragma once
/*
organise all the mode instances and call them at the right time, switch between them
*/
class ModeManager
{
public:
				~ModeManager() {
								Release();
				}
				//Change current mode
				void SwitchMode(const std::string& newMode);

				//Update current mode
				void Update(float dTime);

				//Render current mode
				void Render(float dTime, DirectX::SpriteBatch& batch);

				//Handle WH_CHAR key messages
				void ProcessKey(char key);

				//Add a new mode via dynamic allocation
				void AddMode(iMode* p);

				//Release mode resources, called in destructor
				void Release();

private:
				std::vector<iMode*> modes_;	//container of modes
				int currentModeIndex_ = -1;		//the one that is active
				int nextModeIndex_ = -1;		//one that wants to be active
};