#pragma once
#include <stdint.h>
#include "DXAssetManager.h"
#include "SpriteBatch.h"
#include "D3D.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

//Animation sprite effects definer
enum class SprEffects : short unsigned int
{
    NONE = 0, FLIP_HORIZONTAL = 1, FLIP_VERTICAL = 2, FLIP_BOTH = FLIP_HORIZONTAL | FLIP_VERTICAL,
};

//Animation data container, used to store information on animation start, end, anem and ID.
struct AnimData
{
    size_t animID;
    int animStartFrame;
    int animEndFrame;
    int animationSpeed;
    bool isAnimActive = false;
    std::string animName;
};

class Sprite; //Forward declaration for Animator class
class Animator
{
private:
    enum class AnimState {START, STOP, CURRENT};
    int animStart_ = 0, animEnd_ = 0, animCurrent_ = 0;
    float animSpeed_ = 0, animElapsedTime_ = 0;
    bool loopAnim_ = false;
    bool playAnim_ = false;
    bool reverseAnim_ = false;
    Sprite& spr_;
    AnimData* currentAnim;

    //Animation Data container
    std::vector<AnimData> anims_;

public:
    Animator(Sprite& spr)
        :spr_(spr), currentAnim(nullptr), reverseAnim_(false), playAnim_(false), loopAnim_(false)
    {}
    ~Animator()
    { anims_.clear(); }

    //Initialise sprite with simple start and end animation
    void Initialise(int start, int end, float speed, bool loop, bool reverseAnim = false);
    // Update animation frame
    void Update(float elapsedSec);
    //Start and Stop
    void Play(bool play) { playAnim_ = play; }
    //Set the the animation to play in reverse (TRUE = Reverse, FALSE = Forward) 
    void SetAnimationReverse(bool isReversed);
    //Get the current start frame
    const int GetStartFrame() const { return animStart_; }
    //Get the current ending frame
    const int GetEndFrame() const { return animEnd_; }
    //Get the current active frame
    const int GetCurrentFrame() const { return animCurrent_; }
    //Set new starting frame
    void SetStartFrame(int newStartFrame) { animStart_ = newStartFrame; }
    //Set new ending frame
    void SetEndFrame(int newEndFrame) { animEnd_ = newEndFrame; }
    //Set a new frame manually
    void SetFrame(int frameID);
    void SetAnimationSpeed(float newAnimSpeed) { animSpeed_ = newAnimSpeed; }
    const float GetAnimationSpeed() const { return animSpeed_; }

    //Insert new animation using container
    void AddNewAnimation(AnimData newAnim);
    //Insert new animation using list of arguments instead of container
    void AddNewAnimation(int newAnimStart, int newAnimEnd, int newAnimSpeed, std::string newAnimName);
    //Set the current animation via ID and optionally define animation settings
    void SetAnimation(int indexID, bool play = true, bool loop = true, bool reverseAnim = false);
    //Get information on the currently running animation
    const AnimData* GetCurrentAnimation() const { return currentAnim; }


    //Overloads
    Animator& operator=(const Animator& rSide);

};


/*
    Self contained sprite object for use with DX11. Multiple accessor and mutator functions to alter
    a sprite as you might expect, complete with animatior component.
*/
class Sprite
{
public:
    //Init all data objects and pass references
    Sprite(MyD3D& d3d)
        :d3d_(d3d), texData_(nullptr), tex_(nullptr), texRect_{ 0, 0, 0, 0 },
        pos_(0, 0), scale_(1, 1), origin_(0, 0), rot_(0), screenSize_(0, 0),
        colour_(1, 1, 1, 1), depth_(1), spriteEffectID_(SprEffects::NONE),
        vel_(0, 0), animation_(*this)
    {}
    //Need to pass references this way
    Sprite(const Sprite& rSide)
        :d3d_(rSide.d3d_), animation_(*this)
    {
        *this = rSide;
    }
    ~Sprite()
    {}   

    void Draw(SpriteBatch& batch);

    void SetTexture(ID3D11ShaderResourceView& tex, const RECTF& texRect = RECTF{ 0, 0, 0, 0 });
    void SetTextureRect(const RECTF& texRect);
    void SetPosition(Vector2& pos);
    void SetVelocity(const Vector2& vel);
    void SetScale(const Vector2& scale);
    void SetOrigin(const Vector2& origin);
    void SetColour(const Vector4& colour);
    void SetRotation(const float rot);
    void SetDepth(const float depth);
    void SetSpriteEffect(SprEffects effects);
    void SetFrame(int frameID);
     
    void ScrollTexture(float x, float y);

    Vector2 GetPosition() const;
    Vector2 GetVelocity() const;
    Vector2 GetScale() const;
    Vector2 GetOrigin() const;
    float GetRotation() const;
    float GetDepth() const;
    Vector4 GetColour() const;
    Vector2 GetTextureRect() const;
    Vector2 GetScreenSize() const;

    Vector4 GetCollisionValues() const;


    Animator& GetAnimation()
    {
        return animation_;
    }
    MyD3D& GetD3D()
    {
        return d3d_;
    }
    const DXAssetManager::Data& GetTextureData() const
    {
        assert(texData_);
        return *texData_;
    }
    ID3D11ShaderResourceView& GetTexture()
    {
        assert(tex_);
        return *tex_;
    }

    Sprite& operator=(const Sprite& rhs);

private:
    //Data
    Animator animation_;
    RECTF texRect_;
    Vector4 colour_;
    Vector2 pos_;
    Vector2 vel_;
    Vector2 scale_;
    Vector2 origin_;
    Vector2 screenSize_;
    ID3D11ShaderResourceView* tex_;
    MyD3D& d3d_;
    const DXAssetManager::Data* texData_;
    float rot_;
    float depth_;
    SprEffects spriteEffectID_;

    void UpdateScreenSize();    //Called during SetScale, ensures when screenSize is called its up-to-date.

};