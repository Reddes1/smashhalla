#pragma once

#include "MainStageMode.h"      //Mode

#include "EntityInterface.h"    //Object Parent
#include "Sprite.h"             //Sprite class
#include "Player.h"
#include "GameConstants.h"
class Health : public iEntity
{
public:
    Health();
    ~Health();
    void Update(float dTIme) override;
    void Render(float dTime, DirectX::SpriteBatch& batch) override;

    void ProcessHealthBar1(Health* bar1, Player* player1);
    void ProcessHealthBar2(Health* bar2, Player* player2);

private:
    void Initialise();
    void HealthBarInit();
};

