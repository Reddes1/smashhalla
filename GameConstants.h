#pragma once

/*
    GC = Game Constants. Anything specifically relating to gameplay orentatied variables like speeds, sizes, durations,
    hp etc. 
*/
namespace GC
{

    //////////////////////
    ///Player Constants///
    //////////////////////

    const int MAX_PLAYERS = 2;

    //////////////////////
    ///Player Constants///
    //////////////////////

    //Player Movespeed/jump speed etc
    const float P_MOVE_SPEED = 400.f;
    const float P_JMP_SPEED = 1150.f;
    const float P_KNOCK_BACK_SPEED = 450.f;

    //Damage Values
    const float P_ATTACK1_DMG = 8;
    const float P_ATTACK2_DMG = 5;
    const float P_ATTACK3_DMG = 6;

    //Starting values
    const int P_STARTING_HP = 100;
    const int P_STARTING_LIVES = 3;

    //Physics
    const float P_GRAVITY = 400;

    //Durations
    const float P_JMP_DURATION = 0.7f;
    const float P_STAGGER_WINDOW = 1.f;
    const float P_STAGGER_DURATION = 0.33f;
    const float P_RESPAWN_TIME = 2.f;


    //////////////////////
    ///Stage One Consts///
    //////////////////////

    //Physics (Gravity etc)
    const float G_GRAVITY = 575;
    
}


/*
    FC = File Constants. Things like layer counts, sprite counts, sprite res's. Things that relate to the files or
    the number of things we want when initialising the program.
*/
namespace FC
{
    //Default resolution we want to scale most if not all the sprites to.
    const Vector2 DEFAULT_RES = { 1920, 1080 };

    //////////////////////
    ///Player Constants///
    //////////////////////

    const Vector2 defCollision = { 46.f, 126.f };

    ///////////////////////
    ///Splash Mode Const///
    ///////////////////////

    const int SPLASH_BG_COUNT = 1;

    ///////////////////////
    ///Intro  Mode Const///
    ///////////////////////



    //////////////////////////
    ///Main Menu Mode Const///
    //////////////////////////

    const int MAIN_MENU_BG_COUNT = 1;
    const int MENU_BUTTON_COUNT = 2;


    //////////////////////
    ///Stage One Consts///
    //////////////////////

    //HP bar position values
    const float HP_1X = 0.14f; // player 1 health bar X position
    const float HP_1Y = 0.21f; // player 1 health bar Y position
    const float HP_2X = 0.87f; // player 2 health bar X position
    const float HP_2Y = 0.22f; // player 2 health bar Y position
    //life counter position values
    const float LC_1X = 0.11f; // player 1 life count X position
    const float LC_1Y = 0.235f; // player 1 life count Y position
    const float LC_2X = 0.9f; // player 2 life count X position
    const float LC_2Y = 0.243f; // player 2 life count Y position
    //Health frames postion values
    const float HF_1X = 0.1f; // player 1 health frame X position
    const float HF_1Y = 0.15f; // player 1 health frame Y position
    const float HF_2X = 0.88f; // player 2 health frame X position
    const float HF_2Y = 0.15f; // player 2 health frame Y position
    //////////////////////////
    ///Game Over Mode Const///
    //////////////////////////



    //////////////////////////
    ///Highscore Mode Const///
    //////////////////////////



}

/*
    MC = Mode Constants. Mode specific variables like timers for transitions, duration of something being on screen for
    the mode (slideshow maybe?).
*/
namespace MC
{

    ///////////////////////
    ///Splash Mode Const///
    ///////////////////////

    const float SPLASH_1_DURATION = 2.f;

    ///////////////////////
    ///Intro  Mode Const///
    ///////////////////////


    //////////////////////////
    ///Main Menu Mode Const///
    //////////////////////////


    //////////////////////
    ///Stage One Consts///
    //////////////////////

    //////////////////////
    ///Game Over Consts///
    //////////////////////


    ///////////////////////
    ///Highscores Consts///
    ///////////////////////


}

/*
    FD = Frame Data. Commonly shared frame data that use the same atlas stored in the same location. 
*/
namespace FD
{
    const RECTF backgroundAtlasFrames[]
    {
        {0, 0, 1919, 1079},         //Intro Screen
        {0, 1100, 1919, 2178},      //Main Menu Screen
        {0, 2199, 1919, 3279},      //HighScore Screen
        {1950, 0, 3869, 1079},      //Gameover Screen
        {1950, 1100, 3869, 2178},   //Pause Screen
        {1950, 2199, 3869, 3279}    //HowToPlay Overlay
    };

    const RECTF redPlatformAtlasFrames[]
    {
        {0, 0, 530, 49},    //Main Platform
        {0, 49, 243, 74},   //Sub Platform 1
        {0, 74, 243, 98}    //Sub Platform 2
    };

    const RECTF UIAtlasOneFrames[]
    {
        {0, 0, 803, 172},      //Play
        {0, 170, 803, 341},    //How to Play
        {0, 340, 803, 511},    //Exit
        {70, 755, 460, 1000},  //HP Frame 1
        {100, 517, 504, 748},  //HP Frame 2
        {740, 592, 966, 600},  //Life Bar 1
        {756, 562, 850, 572},  //Lives Bar 1
    };

}