#pragma once
//Collection of simple flag structures that may be useful in other objects
/*
    Set of 8 generic flags.
*/
struct GenericFlags8
{
    GenericFlags8()
        :flag1(false), flag2(false), flag3(false), flag4(false),
        flag5(false), flag6(false), flag7(false), flag8(false)
    {}
    bool flag1 : 1;
    bool flag2 : 1;
    bool flag3 : 1;
    bool flag4 : 1;
    bool flag5 : 1;
    bool flag6 : 1;
    bool flag7 : 1;
    bool flag8 : 1;
};
/*
    Set of 16 generic flags.
*/
struct GenericFlags16
{
    GenericFlags16()
        :flag1(false), flag2(false), flag3(false), flag4(false),
        flag5(false), flag6(false), flag7(false), flag8(false),
        flag9(false), flag10(false), flag11(false), flag12(false),
        flag13(false), flag14(false), flag15(false), flag16(false)
    {}
    bool flag1 : 1;
    bool flag2 : 1;
    bool flag3 : 1;
    bool flag4 : 1;
    bool flag5 : 1;
    bool flag6 : 1;
    bool flag7 : 1;
    bool flag8 : 1;
    bool flag9 : 1;
    bool flag10 : 1;
    bool flag11 : 1;
    bool flag12 : 1;
    bool flag13 : 1;
    bool flag14 : 1;
    bool flag15 : 1;
    bool flag16 : 1;
};
/*
    Input orentated flagset. Stick movement, up/down/left/right & 10 action flags
*/
struct InputFlags
{
    InputFlags()
        : isLeftStickMoved(false), isRightStickMoved(false), isUpPressed(false),
        isDownPressed(false), isLeftPressed(false), isRightPressed(false),
        isAction1Pressed(false), isAction2Pressed(false), isAction3Pressed(false),
        isAction4Pressed(false), isAction5Pressed(false), isAction6Pressed(false),
        isAction7Pressed(false), isAction8Pressed(false), isAction9Pressed(false),
        isAction10Pressed(false)
    { }
    bool isLeftStickMoved : 1;
    bool isRightStickMoved : 1;
    bool isUpPressed : 1;
    bool isDownPressed : 1;
    bool isLeftPressed : 1;
    bool isRightPressed : 1;
    bool isAction1Pressed : 1;
    bool isAction2Pressed : 1;
    bool isAction3Pressed : 1;
    bool isAction4Pressed : 1;
    bool isAction5Pressed : 1;
    bool isAction6Pressed : 1;
    bool isAction7Pressed : 1;
    bool isAction8Pressed : 1;
    bool isAction9Pressed : 1;
    bool isAction10Pressed : 1;

};
/*
    Useful named flags for the state of an object/player/enemy like direction faced, moving, jumping etc.
*/
struct MovDirFlags
{
    MovDirFlags()
        :movingUp(false), movingDown(false), movingLeft(false),
        movingRight(false), movingUpLeft(false), movingUpRight(false),
        movingDownLeft(false), movingDownRight(false), isMoving(false),
        isLookingUp(false), isLookingDown(false), isLookingLeft(false),
        isLookingRight(false), isJumping(false), isFalling(false),
        isStationary(false), isAttacking(false)
    { }
    bool movingUp : 1;
    bool movingDown : 1;
    bool movingLeft : 1;
    bool movingRight : 1;
    bool movingUpLeft : 1;
    bool movingUpRight : 1;
    bool movingDownLeft : 1;
    bool movingDownRight : 1;
    bool isLookingUp : 1;
    bool isLookingDown : 1;
    bool isLookingLeft : 1;
    bool isLookingRight : 1;
    bool isMoving : 1;
    bool isJumping : 1;
    bool isFalling : 1;
    bool isStationary : 1;
    bool isAttacking : 1;
};
/* 
    State based flags that may describe what has happened to an object.
*/
struct ObjStateFlags
{
    ObjStateFlags() 
        : isDead(false), isDamaged(false), isHealed(false), canAttack(false)
    {}
    bool isDead : 1;
    bool isDamaged : 1;
    bool isHealed : 1;
    bool canAttack : 1;
};