#include "TestObject.h"

TestObject::TestObject()
    : iEntity(Game::Get().GetD3D())
{
    Initialise();
}

void TestObject::Update(float dTime)
{

}

void TestObject::Render(float dTime, DirectX::SpriteBatch& batch)
{
    iEntity::Render(dTime, batch);
}

void TestObject::ResetVariables()
{

}

void TestObject::Initialise()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Update scaling values to current screen size
    UpdateScalingAdjustments();
    
    //Load the Texture
    std::string filePath = "data/sprites/PolygonTest.dds";

    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(),
        filePath, "Test Object", false);

    //Set object parameters
    spr_.SetTexture(*p);
    spr_.SetOrigin(Vector2(spr_.GetTextureRect().x / 2, spr_.GetTextureRect().y / 2));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 1, adj_.scaleAdj.y * 1));
    spr_.SetPosition(Vector2(width / 2.f, height / 2.f));

    //Store vertex data and adjust scaling
    for (int i(0); i < 5; ++i)
    {
        //Push vertex data into container
        vertexData_.push_back(verticeData[i]);
        
        //Scale down the data to match the on screen size
        vertexData_[i] *= adj_.scaleAdj;
        //Align the vertices with the sprites origin.
        vertexData_[i] -= spr_.GetScreenSize() * 0.5;
    }



}
