#include "PlatformObj.h"
#include "GameConstants.h"

PlatformObj::PlatformObj()
    : iEntity(Game::Get().GetD3D())
{
    Initialise();
}

void PlatformObj::Update(float dTime)
{
    colData_.collisionBox2D.x = spr_.GetScreenSize().x * spr_.GetScale().x;
    colData_.collisionBox2D.y = spr_.GetScreenSize().y * spr_.GetScale().y;

}

void PlatformObj::Render(float dTime, DirectX::SpriteBatch& batch)
{
    iEntity::Render(dTime, batch);
}

void PlatformObj::ResetVariables()
{

}

void PlatformObj::CalculateSidesData()
{
    Vector2 halfSize = colData_.collisionBox2D * 0.5f;
    Vector2 pos = spr_.GetPosition();

    sidesData_.left = pos.x - halfSize.x;
    sidesData_.right = pos.x + halfSize.x;
    sidesData_.top = pos.y - halfSize.y;
    sidesData_.bottom = pos.y + halfSize.y;
}

void PlatformObj::Initialise()
{
    //Handy Handles
    MyD3D& d3d = Game::Get().GetD3D();
    WinUtil& win = WinUtil::Get();
    //Capture screen size
    int width = win.GetData().clientWidth;
    int height = win.GetData().clientHeight;

    //Update scaling values to current screen size
    UpdateScalingAdjustments();

    //Get Frames
    std::vector<RECTF> frames(FD::redPlatformAtlasFrames,
        FD::redPlatformAtlasFrames + sizeof(FD::redPlatformAtlasFrames) / sizeof(FD::redPlatformAtlasFrames[0]));

    //Load the Texture
    std::string filePath = "data/sprites/platformatlas.dds";
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(),
        filePath, "Platform Atlas", false, &frames);

    //Set object parameters
    spr_.SetTexture(*p);
    spr_.SetTextureRect(frames[0]);
    spr_.SetOrigin(Vector2(frames[0].right * 0.5f, frames[0].bottom * 0.5f));
    spr_.SetScale(Vector2(adj_.scaleAdj.x * 1.1f, adj_.scaleAdj.y * 1.1f));
    spr_.SetPosition(Vector2(width * 0.5f, height * 0.5f));

    //Set Collision Data
    colData_.collisionBox2D.x = spr_.GetScreenSize().x * spr_.GetScale().x;
    colData_.collisionBox2D.y = spr_.GetScreenSize().y * spr_.GetScale().y;

    CalculateSidesData();

}

