#include "HealthDisplay.h"

HealthDisplay::HealthDisplay()
	:iEntity(Game::Get().GetD3D())
{
	Initialise();
}

HealthDisplay::~HealthDisplay()
{
}

void HealthDisplay::Update(float dTime)
{
}

void HealthDisplay::Render(float dTime, DirectX::SpriteBatch& batch)
{
	iEntity::Render(dTime, batch);
}

void HealthDisplay::ProcessHealth1(HealthDisplay* bar1, Player* player1)
{
}

void HealthDisplay::ProcessHealth2(HealthDisplay* bar2, Player* player2)
{
}

void HealthDisplay::ProcessLife1(HealthDisplay* life1, Player* player1)
{
}

void HealthDisplay::ProcessLife2(HealthDisplay* life2, Player* player2)
{
}

void HealthDisplay::Initialise()
{
	HealthSetup();
}

void HealthDisplay::HealthSetup()
{
    //Reserve space
    healthDisplay_.reserve(4);

    //Handy Handles
    Game& game = Game::Get();
    MyD3D& d3d = game.GetD3D();
    WinUtil& winUtil = WinUtil::Get();

    //Get screen dims
    int width = winUtil.GetData().clientWidth;
    int height = winUtil.GetData().clientHeight;

    //Use the iEntity sprite as a ghost container and set its parameters
    spr_.SetTextureRect(RECTF(0, 0, width, height));
    spr_.SetScale(Vector2(width / spr_.GetTextureRect().x, height / spr_.GetTextureRect().y));
    spr_.SetOrigin(Vector2(spr_.GetTextureRect() * 0.5f));
    spr_.SetPosition(Vector2(width * 0.5f, height * 0.5f));

    healthDisplay_.insert(healthDisplay_.begin(), FC::HP_FRAME_COUNT, Sprite(game.GetD3D()));

    //Load texture and frames
    std::vector<RECTF> frames(healthFrames, healthFrames + sizeof(healthFrames) / sizeof(healthFrames[0]));
    ID3D11ShaderResourceView* p = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "data/UI/UI_Elements2.dds",
        "Health frames", false, &frames);

    //configure
    for (int i(0); i < FC::HP_FRAME_COUNT; ++i)
    {
        healthDisplay_[i].SetTexture(*p);
        healthDisplay_[i].SetOrigin(Vector2(frames[0].right * 0.5f, frames[0].bottom * 0.5f));
        healthDisplay_[i].SetScale(spr_.GetScale());
        healthDisplay_[i].SetPosition(Vector2(width * 0.1, height * 0.1));
    }
}
